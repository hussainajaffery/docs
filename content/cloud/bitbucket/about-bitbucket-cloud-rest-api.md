---
title: "About Bitbucket Cloud REST API"
platform: cloud
product: bitbucketcloud
category: reference
subcategory: api 
date: "2017-05-22"
---

# About Bitbucket Cloud REST API

The Bitbucket Cloud REST API allows you to build apps using any language you want. Using the API, users can sign in and grant your app the right to make calls on their behalf. Then, through the API, your app can access Bitbucket Cloud resources such as individual (or team) accounts, repositories, and aspects of these resources such as changesets or comments.

You should be familiar with REST architecture before writing an integration. Good REST resources abound on the Internet. Read this overview page to gain a good understanding of Bitbucket's REST implementation. When you are ready to begin, [obtain a consumer key for your application](/cloud/bitbucket/oauth-2).
