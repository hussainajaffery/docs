---
title: "App descriptor"
platform: cloud
product: bitbucketcloud
category: devguide
subcategory: learning
learning: guides 
date: "2018-04-03"
---

# App descriptor

The descriptor is a JSON object that provides the base upon which connect apps are built. The descriptor provides the structure, connection, and permissions for the app you develop. 

Essentially when a user installs an app they are installing the JSON descriptor object. 

We've broken up the descriptor from our example app into sections to describe all the elements of the descriptor in context. The [full descriptor example ](#full-example) is at the bottom of the page. It should not be considered an exhaustive list of all the available elements and properties.

### Key, name, description, and vendor elements <a name="k-n-d-v"></a>

<table class='aui'>
    <thead>
        <tr>
            <th>Element</th>
            <th>Description</th>
            <th>Required</th>
        </tr>
    </thead>
    <tr>
        <td><code>key</code></td><td>A unique text string to identify the app</td><td>Yes</td>
    </tr>
    <tr>
        <td><code>name</code></td><td>A text string with the name of the app for display</td><td>Yes</td>
    </tr>
    <tr>
        <td><code>description</code></td><td>A sentence describing your app</td><td>Yes</td>
    </tr>
    <tr>
        <td><code>vendor</code></td><td>The vendor field is available to show your company name and a URL. It has the following parameters:<ul><li><code>name</code>: The name of your company or team.</li><li><code>url</code>: Any URL you choose to link to your company or team.</li></ul></td><td>No</td>
    </tr>
</table>

``` json
{
    "key": "example-app",
    "name": "Example App",
    "description": "An example app for Bitbucket",
    "vendor": {
        "name": "Angry Nerds",
        "url": "https://www.example.com"
    },
```

### baseUrl and authentication <a name="base-auth"></a>


<table class='aui'>
    <thead>
        <tr>
            <th>Element</th>
            <th>Description</th>
            <th>Required</th>
        </tr>
    </thead>
    <tr>
        <td><code>baseUrl</code></td><td>The root or base URL from which all other relative URL's will derive. Must be an https://-based URL.</td><td>Yes</td>
    </tr>
    <tr>
        <td><code>authentication</code></td><td>The <a href="/cloud/bitbucket/authentication-for-apps">authentication</a> method your app will use. 
  <br>It has the following parameter: <ul><li><code>type</code>: must be either <code>jwt</code> or <code>none</code></li></ul></td><td>Yes</td>
    </tr>
</table>

``` json
    "baseUrl": "https://example.com",
    "authentication": {
        "type": "jwt"
    },
```
### lifecycle <a name="lifecycle"></a>

<table class='aui'>
    <thead>
        <tr>
            <th>Element</th>
            <th>Description</th>
            <th>Required</th>
        </tr>
    </thead>
    <tr>
        <td><code>lifecycle</code></td><td>Each property in this object is a URL that can be absolute or relative to the app's baseUrl. <br>When a lifecycle event is fired, Bitbucket will POST to the appropriate URL registered for the event. <br>Can have one of the following values:
        <ul><li><code>installed</code>: contains the URL or relative URL to POST to when the app is installed.</li>
            <li><code>uninstalled</code>: contains the URL or relative URL to POST to when the app is uninstalled</li></ul>
    </td><td>Yes</td>
    </tr>
</table>

``` json
    "lifecycle": {
        "installed": "/installed",
        "uninstalled": "/uninstalled"
    },
```
### Scopes and contexts <a name="scope-context"></a>

Scopes define which access permissions your app will require or request from Bitbucket. The scopes here are just an example of how they might be applied in the descriptor, the full list of scopes is extensive and available at: [Bitbucket Cloud REST API scopes](/cloud/bitbucket/bitbucket-cloud-rest-api-scopes/). 

Contexts here are [app contexts](/cloud/bitbucket/app-context) which determine the level at which an app is visible and active once a user installs the app. 
<table class='aui'>
    <thead>
        <tr>
            <th>Element</th>
            <th>Description</th>
            <th>Required</th>
        </tr>
    </thead>
    <tr>
        <td><code>scopes</code></td><td> Define the access permissions your app will require or request from Bitbucket.<br>Has the following parameters:
        <ul>
            <li><code>account</code>: Ability to see all the user's account information. Note that this does not include any ability to mutate any of the data.</li>
            <li><code>repository</code>: read access to all the repositories the authorizing user has access to. Note that this scope does not give access to a repository's pull requests.</li>
        </ul>
    </td><td>Yes</td>
    </tr>
    <tr>
        <td><code>contexts</code></td><td>App contexts determine the account level at which an app is installed. <br>Can have one of the following values:
        <ul>
            <li><code>account</code>: the app will be visible in all the locations that the owner of the installing account has access.</li>
            <li><code>individual</code>: the app will only be visible to the owner of the individual account.</li>
        </ul>
    </td><td>Yes</td>
    </tr>
</table>

``` json
    "scopes": ["account", "repository"],
    "contexts": ["account"]
}
```
### modules <a name="modules"></a>

 Modules are the specific integration points implemented by your app. These include UI elements like pages, web panels, and web items. Other types of modules do not modify the Bitbucket UI, but describe how your app interacts programmatically with Bitbucket, such as webhooks and Oauth consumers.

``` json
    "modules": {
        "oauthConsumer": {
            "clientId": "{{consumerKey}}"
        },
        "webhooks": [
            {
                "event": "*",
                "url": "/webhook"
            }
        ],
        "webItems": [
            {
                "url": "http://example.com?repoPath={repo_path}",
                "name": {
                    "value": "Example Web Item"
                },
                "location": "org.bitbucket.repository.navigation",
                "key": "example-web-item",
                "params": {
                    "auiIcon": "aui-iconfont-link"
                }
            }
        ],
        "repoPages": [
            {
                "url": "/connect-example?repoPath={repo_path}",
                "name": {
                    "value": "Example Page"
                },
                "location": "org.bitbucket.repository.navigation",
                "key": "example-repo-page",
                "params": {
                    "auiIcon": "aui-iconfont-doc"
                }
            }
        ],
        "webPanels": [
            {
                "url": "/connect-example?repoPath={repo_path}",
                "name": {
                    "value": "Example Web Panel"
                },
                "location": "org.bitbucket.repository.overview.informationPanel",
                "key": "example-web-panel"
            }
        ]
    },
}
```

### Definitions <a name="definitions"></a>

The <code>definitions</code> section of the descriptor enables
application developers to define their own object schemas. Currently
the only place such schemas can be used is when they're referenced from
a [custom event](/cloud/bitbucket/modules/custom-events/).

``` json
    "definitions": {
        "com.example.app:myobject": {
            "type": "object",
            "description": "An object",
            "properties": {
                "my-property": {
                    "type": "string",
                    "description": "A property",
                    "required": true
                },
                "repository": {
                    "$ref": "#/definitions/repository"
                }
            }
        }
    }
```

Definition schemas should use [JSON Schema](http://json-schema.org)
Draft 4 syntax. They may reference other definitions created by the
application, or even native Bitbucket object types like
<code>repository</code>.

Prefix object definition keys with the key of the application in order
to namespace the definitions, since validation will ensure that this is
happening.

## Complete descriptor example <a id="full-example"></a>

Our example app descriptor provides a good example of all the required parts
of the descriptor you will need to construct for your app.

``` json
{
    "key": "example-app",
    "name": "Example App",
    "description": "An example app for Bitbucket",
    "vendor": {
        "name": "Angry Nerds",
        "url": "https://www.atlassian.com/angrynerds"
    },
    "baseUrl": "https://example.com",
    "authentication": {
        "type": "jwt"
    },
    "lifecycle": {
        "installed": "/installed",
        "uninstalled": "/uninstalled"
    },
    "modules": {
        "oauthConsumer": {
            "clientId": "{{consumerKey}}"
        },
        "webhooks": [
            {
                "event": "*",
                "url": "/webhook"
            }
        ],
        "webItems": [
            {
                "url": "http://example.com?repoPath={repo_path}",
                "name": {
                    "value": "Example Web Item"
                },
                "location": "org.bitbucket.repository.navigation",
                "key": "example-web-item",
                "params": {
                    "auiIcon": "aui-iconfont-link"
                }
            }
        ],
        "repoPages": [
            {
                "url": "/connect-example?repoPath={repo_path}",
                "name": {
                    "value": "Example Page"
                },
                "location": "org.bitbucket.repository.navigation",
                "key": "example-repo-page",
                "params": {
                    "auiIcon": "aui-iconfont-doc"
                }
            }
        ],
        "webPanels": [
            {
                "url": "/connect-example?repoPath={repo_path}",
                "name": {
                    "value": "Example Web Panel"
                },
                "location": "org.bitbucket.repository.overview.informationPanel",
                "key": "example-web-panel"
            }
        ]
    },
    "scopes": ["account", "repository"],
    "contexts": ["account"]
}
```
