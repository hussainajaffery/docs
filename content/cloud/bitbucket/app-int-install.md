---
title: "Application initiated app installation"
platform: cloud
product: bitbucketcloud
category: devguide
subcategory: intro
date: "2016-03-28"
---

# Application initiated app installation

Bitbucket supports initiating an app installation from a third party application so long as the app descriptor is publicly available. 

![Application initiated app installation diagram](/cloud/bitbucket/images/app-int-install-diagram.png)

(A) The app initiates the flow by sending the user-agent to the Bitbucket authorization endpoint. The app includes the URI of its descriptor or an app key and a redirection URI to which Bitbucket will send the user-agent once the installation is complete or denied.

(B) Bitbucket authenticates the user, allows them to select the account (individual or team) where they want to install the app, and presents them with the requested scopes (access permissions).

(C) When the end user grants installation, Bitbucket redirects the user back to the app using the redirect URI and includes the client key for the app installation (passed to the app during the 'installed' lifecycle callback)

## Authorization Request

Sending the User-Agent to the Bitbucket app authorization endpoint initiates the installation. You must specify either a `descriptor_uri` or an `addon_key` parameter. An `addon_key` can only be used for apps that are currently listed in the Bitbucket marketplace.

Endpoint:
http://bitbucket.org/site/addons/authorize

### Parameters
 
  - `redirect_uri` REQUIRED The endpoint the User-Agent will be redirected to this value when the flow is complete. The redirect URI must be a subpath of the baseUrl specified in the app's descriptor.
  - `descriptor_uri` OPTIONAL The URI of an Bitbucket connect descriptor. The descriptor must be accessible by the Bitbucket servers.
  - `addon_key` OPTIONAL The app key of an app listed in the Marketplace.

## Authorization Response

The redirection endpoint URI must be an absolute URI and will include a application/x-www-form-urlencoded query string.

### Parameters
  
  - `client_key` REQUIRED An identifier for the installed app. This can be matched with the value sent by Bitbucket to the app server during the "installed" lifecycle callback. Installation is a synchronous process, by the time the redirect has occurred, the app has received the callback.

## Error Response

If the request fails due to a missing or malformed redirect URI, Bitbucket will inform the end user of the error and app server will receive no notification that an error has occurred.

For all other errors, as in the success case Bitbucket will redirect to the redirect URI, but the parameters will be different.

### Parameters
 
 - error REQUIRED An error code from the following:
    - `invalid_request` The request is missing a required parameter, or a parameter value is invalid.
    - `server_error` An internal error occurred while Bitbucket was processing the installation request.
    - `invalid_descriptor` There's a validation problem with the specified descriptor.
    - `access_denied` The user rejected installation of the app
 - error_description OPTIONAL A human readable description of the error.
 
