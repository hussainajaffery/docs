## Design apps for Bitbucket Cloud

[Design apps](/cloud/bitbucket/design-apps-for-bitbucket-cloud)

Find resources and guidance to design visually appealing apps for Bitbucket Cloud.

## Node.js

[Git guilt](https://bitbucket.org/tpettersen/bitbucket-git-guilt)

App that generates contributor statistics by running git commands against a local cache of clones.

[Bitbucket Repo Cache](https://bitbucket.org/tpettersen/bitbucket-repo-cache)

Experimental library that maintains a local cache of Bitbucket repositories on behalf of an app.

## Static apps

Static apps use only HTML, CSS, and client-side JavaScript.

[Extension Cloud](https://bitbucket.org/tpettersen/bitbucket-extension-cloud)

App that generates a simple word cloud of file extensions by walking repository trees using Bitbucket's REST API.