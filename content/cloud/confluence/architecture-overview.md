---
title: "Architecture overview"
platform: cloud
product: confcloud
category: devguide
subcategory: intro
date: "2017-08-31"
---
{{% include path="docs/content/cloud/connect/concepts/cloud-development.snippet.md" %}}
