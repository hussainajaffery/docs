---
title: "Conditions"
platform: cloud
product: confcloud
category: devguide
subcategory: blocks
date: "2017-10-19"
---
# Conditions

Conditions are a powerful way to control whether your UI elements should be shown or not. A common 
example is to specify that a user must be logged in to see the app. Or, you may want your element 
to always display but behave differently depending on user permissions or other states that 
Conditions allow you to check. 

## Usage

Predefined conditions provided by each host product are the most commonly used conditions. But, if 
you need more flexibility, custom conditions stored by the app in the host product is an option:

* Used in certain modules including Pages, Web Panels, and Web Items. See [module documentation](/cloud/confluence/modules/dialog/) to determine which modules accept them. 
* Used in [context parameters](../context-parameters/#inline-conditions).
* Used as building blocks of [Boolean operations](#boolean-operations) in apps. 


## Predefined conditions

A predefined condition is a condition which is exposed from the host Atlassian application. 

For example, a condition that will evaluate when only logged-in users view the page is specified by 
the following module declaration.

``` javascript
{
    "modules": {
        "generalPages": [
            {
                "conditions": [
                    {
                        "condition": "user_is_logged_in"
                    }
                ]
            }
        ]
    }
}
```

See the [list of predefined conditions](#list-of-predefined-conditions).

### Condition parameters

Certain predefined conditions accept parameters.

For example, the `has_attachment_permission` condition passes only for users who have the permission 
specified in the condition. This means that permissions are checked when the user views the page in 
a browser.

``` javascript
{
    "modules": {
        "generalPages": [
            {
                "conditions": [
                    {
                        "condition": "has_attachment_permission",
                        "params": {
                            "permission": "EDIT"
                        }
                    }
                ]
            }
        ]
    }
}
```

### List of predefined conditions

There are a set of common conditions and Confluence-specific conditions. 

#### Common

* `addon_property_equal_to`
* `addon_property_equal_to_context`
* `addon_property_contains_any`
* `addon_property_contains_all`
* `addon_property_contains_context`
* `addon_property_contains_any_user_group`
* [entity_property_equal_to](#property-conditions)
* `entity_property_equal_to_context`
* `entity_property_contains_any` 
* `entity_property_contains_all`
* `entity_property_contains_context`
* `entity_property_contains_any_user_group`
* `feature_flag`
* `user_is_admin`
* `user_is_logged_in`
* `user_is_sysadmin`
* `addon_is_licensed`


#### Confluence

* `can_edit_space_styles`
* `can_signup`
* `content_has_any_permissions_set`
* `content_property_equal_to`
* `content_property_equal_to_context`
* `content_property_contains_any`
* `content_property_contains_all`
* `content_property_contains_context`
* `content_property_contains_any_user_group`
* `create_content`
* `email_address_public`
* `favourite_page`
* `favourite_space`
* `following_target_user`
* `has_attachment`
* `has_attachment_permission`
* `has_blog_post`
* `has_comment_permission`
* `has_custom_content_permission`
* `has_page`
* `has_page_permission`
* `has_space`
* `has_space_permission`
* `has_template`
* `latest_version`
* `not_personal_space`
* `printable_version`
* `showing_page_attachments`
* `space_function_permission`
* `space_property_equal_to`
* `space_property_equal_to_context`
* `space_property_contains_any`
* `space_property_contains_all`
* `space_property_contains_context`
* `space_property_contains_any_user_group`
* `space_sidebar`
* `target_user_has_personal_blog`
* `target_user_has_personal_space`
* `threaded_comments`
* `tiny_url_supported`
* `user_can_create_personal_space`
* `user_can_use_confluence`
* `user_favouriting_target_user_personal_space`
* `user_has_personal_blog`
* `user_has_personal_space`
* `user_is_confluence_administrator`
* `user_logged_in_editable`
* `user_watching_page`
* `user_watching_space`
* `user_watching_space_for_content_type`
* `viewing_content`
* `viewing_own_profile`

#### <a name="confluence-condition-parameters"></a>Confluence condition parameters

`has_attachment_permission`, `has_comment_permission`, `has_custom_content_permission`, 
`has_page_permission` and `has_space_permission` require a key of the permission that will be 
checked for the current user to be passed via the `permission` parameter. Below you will find the 
keys of the valid permissions. 

##### Permission keys

* `VIEW`
* `EDIT`
* `SET_PERMISSIONS`
* `REMOVE`
* `EXPORT`
* `ADMINISTER`

#### Global permission keys

* `ADMINISTER`
* `SYSTEM_ADMIN`
* `USER_PICKER`
* `CREATE_SHARED_OBJECTS`
* `MANAGE_GROUP_FILTER_SUBSCRIPTIONS`
* `BULK_CHANGE`

### Property conditions

Property conditions operate on properties of the user at the browser, the entity being viewed 
(e.g., a page or a blog post), or its container (e.g., a space), or the entire system.

Apps that need to impose custom requirements when user interface elements are displayed can use 
a property condition.

The following property conditions exist:

 - `entity_property_equal_to`
 - `entity_property_contains_any` 
 - `entity_property_contains_all` 

These allow fast comparisons to be made against data (properties) stored by the app in the host 
product. See the [Confluence entity properties](../confluence-entity-properties) page for 
more details. Usually, properties are set by a REST call against an entity type. See the 
[Confluence Cloud REST API](/cloud/confluence/rest/) documentation for information about how to 
manage properties for each entity.

Property conditions are defined in the `atlassian-connect.json` file in a similar way to built in 
conditions. An example is below.

``` javascript
{
    "modules": {
        "webItems": [
            {
                "key": "conditional-web-item",
                "name": {
                    "value": "Conditional web item"
                },
                "url": "/conditional-web-item",
                "location": "page.metadata.banner",
                    {
                "conditions": [
                        "condition": "entity_property_equal_to",
                        "params": {
                            "entity": "content",
                            "propertyKey": "example-key",
                            "value": "example-value",
                            "objectName": "state"
                        }
                    }
                ]
            }
        ]
    }
}
```

The condition requires three parameters and can contain (as in the above example) an optional 
fourth parameter.

#### Required parameters

* `entity` - the entity on which the property has been stored. If an entity of the expected type is 
not present in the 
  rendering context of the user interface element, the condition evaluates to false.
  * [addon](/cloud/confluence/confluence-entity-properties/)
  * `content`, `space`
* `propertyKey` - the key of the property to check. If the property is not present, the condition 
evaluates to false.
* `value` - the value to compare the property value against.

#### Optional parameters

The optional parameter `objectName` lets you select which part of the entity property JSON value 
you wish to compare 
against. For example, if an entity property had the following value:

``` json 
{
    "one": {
        "ignored": "value",
        "two": true
    },
    "also": "ignored"
}
```    

Then you could set the `value` parameter to `true` and the `objectName` parameter to `one.two` and 
the condition would evaluate to `true`. Specifying an `objectName` string which cannot be used to 
traverse the property value results in `false`.

#### entity_property_equal_to example

An app could let administrators activate functionality per Confluence space. This could be achieved 
by storing an entity property with the key `mySettings` and the value `{"isEnabled" : true}` on 
each project using the product's REST API. Then use the `entity_property_equal_to` to test for it 
with the following module definition:

``` json
{
    "modules": {
        "webPanels": [
            {
                "location": "atl.confluence.view.space.right.context",
                "conditions": [
                    {
                        "condition": "entity_property_equal_to",
                        "params": {
                            "entity": "space",
                            "propertyKey": "mySettings",
                            "objectName": "isEnabled",
                            "value": "true"
                        }
                    }
                ]
            }
        ]
    }
}
```

#### entity_property_contains_any example

It is also possible to write conditions where you can check if one or all of the values in the 
condition are present in the hosted data.

For example, imagine that you stored the following data against a Confluence space under the property 
key `consumingTeams`:

``` json
{
    "teamCount": 4,
    "teams": ["Legal", "Human Resources", "Accounting", "Taxation"]
}
    
```    

You could then write a condition to only apply to projects that are for the `Accounting` and 
`Development` teams, like so:

``` json
{
    "condition": "entity_property_contains_any",
    "params": {
        "entity": "space",
        "propertyKey": "consumingTeams",
        "objectName": "teams",
        "value": "[\"Accounting\", \"Development\"]"
    }
}
```

You will note that the `value` parameter needs to be a string escaped JSON element. 
-->

### Property conditions aliases

The following aliases can be used which make writing property conditions more convenient.

* `addon_property_equal_to`
* `addon_property_contains_any`
* `addon_property_contains_all`
* `content_property_equal_to`
* `content_property_contains_any`
* `content_property_contains_all`
* `space_property_equal_to`
* `space_property_contains_any`
* `space_property_contains_all`

When using these aliases, the `entity` param is not used. This would mean the following two 
condition definitions are identical.

This definition...

``` json
{
	"condition": "entity_property_equal_to",
	"params": {
		"entity": "content",
		"propertyKey": "flagged",
		"value": "true"
	}
}
```

...is the same as this definition.

``` json
{
	"condition": "content_property_equal_to",
	"params": {
		"propertyKey": "flagged",
		"value": "true"
	}
}
```

### addon_is_licensed condition

The `addon_is_licensed` condition will evaluate to `true` if and only if your app is a paid 
app and it is licensed. This condition can be placed on any Atlassian Connect module that 
supports conditions, and it is not context sensitive. Here is an example of the new condition in use:

``` json
"generalPages": [{
    "conditions": [
        {
            "condition": "addon_is_licensed"
        },
        {
            "condition": "user_is_logged_in"
        }
    ], 
    "key": "your-module-key", 
    "name": {
        "value": "Your module name"
    }, 
    "url": "/panel/for/page", 
    "weight": 100
}]
```    

In this example, the Jira Issue Tab Panel will only be shown if the app is licensed and the user 
is logged in.

{{% note %}}Only use this condition with paid apps that are licensed via the Atlassian Marketplace. If you give away your app for *free* then you must not use the `addon_is_licensed` condition. This is important because all free apps are considered unlicensed and thus the condition will return `false`.{{% /note %}}

## Boolean operations

The [composite condition](../modules/composite-condition/) 
module fragment can be used wherever a condition is expected. This allows the construction of 
boolean expressions aggregating multiple conditions.

For example, a condition that will evaluate when only anonymous users view the page is specified by 
the following module declaration.

``` json
{
    "modules": {
        "generalPages": [
            {
                "conditions": [
                    {
                        "condition": "user_is_logged_in",
                        "invert": false
                    }
                ]
            }
        ]
    }
}
```