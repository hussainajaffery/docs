---
title: "Custom content with Confluence Connect"
platform: cloud
product: confcloud
category: devguide
subcategory: learning
guides: tutorials
date: "2019-01-25"
aliases:
- confcloud/custom-content-with-confluence-connect-40511954.html
- /confcloud/custom-content-with-confluence-connect-40511954.md
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=40511954
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=40511954
confluence_id: 40511954
---

# Custom content with Confluence Connect

<table>
    <colgroup>
        <col width="20%">
        <col width="80%">
    </colgroup>
	<tbody>
	<tr>
		<td><strong>Description</strong></td>
		<td>An advanced walkthrough of custom content in Confluence.</td>
	</tr>
	<tr>
		<td><strong>Level</strong></td>
		<td>Intermediate</td>
	</tr>
	<tr>
		<td><strong>Estimated time</strong></td>
		<td>60 minutes</td>
	</tr>
	</tbody>
</table>

## Prerequisites

Ensure you have installed all the tools you need for Confluence Connect app development, and running Confluence by going through the [getting started](/cloud/confluence/getting-started) guide.

## Lessons

In this series, we build a content application which exposes two new content types to Confluence - customers and notes, where notes are contained and stored under customers. 

<table>
	<thead>
		<tr>
			<th>Title</th>
			<th>Description</th>
			<th>Example repository</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><a href="/cloud/confluence/lesson-1-a-new-content-type/">Lesson 1 - A new content type</a></td>
			<td>Set up a new customer content type</td>
			<td><a href="https://bitbucket.org/atlassianlabs/confluence-custom-content-example">https://bitbucket.org/atlassianlabs/confluence-custom-content-example</a></td>
		</tr>
		<tr>
			<td><a href="/cloud/confluence/lesson-2-adding-content-customers-ahoy/">Lesson 2 - Adding content - customers ahoy</a></td>
			<td>Set up and create instances of a new customer content type.</td>
			<td><a href="https://bitbucket.org/atlassianlabs/confluence-custom-content-example">https://bitbucket.org/atlassianlabs/confluence-custom-content-example</a></td>
		</tr>
		<tr>
			<td><a href="/cloud/confluence/lesson-3-extra-searching-capabilities/">Lesson 3 - Extra searching capabilities</a></td>
			<td>Add extra searching capabilities to our customer content type</td>
			<td><a href="https://bitbucket.org/atlassianlabs/confluence-custom-content-example">https://bitbucket.org/atlassianlabs/confluence-custom-content-example</a></td>
		</tr>
		<tr>
			<td><a href="/cloud/confluence/lesson-4-noting-down-information/">Lesson 4 - Noting Down Information</a></td>
			<td>Set up and create instances of a new note content type, contained within customers.</td>
			<td><a href="https://bitbucket.org/atlassianlabs/confluence-custom-content-example">https://bitbucket.org/atlassianlabs/confluence-custom-content-example</a></td>
		</tr>
	</tbody>
</table>