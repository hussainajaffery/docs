---
title: "Changes to Confluence Cloud Search APIs and deprecation notice"
platform: cloud
product: confcloud
category: devguide
subcategory: updates
date: "2019-07-17"
---

# Changes to Confluence Cloud Search APIs and deprecation notice

{{% note %}}
We are making changes to the existing `/wiki/rest/api/search` REST API to not support user search starting **20 Jan 2019**. We have introduced a new user search REST API (`/wiki/rest/api/search/user`) that you can use today
{{% /note %}}

As a part of larger
[GDPR changes](https://confluence.atlassian.com/cloud/blog/2019/05/upcoming-changes-to-atlassian-account-profiles),
we are making some changes to our `/wiki/rest/api/search` API. Starting 20 Jan
2019, we will no longer support user search through /wiki/rest/api/search.

Along with this you will also notice the following changes (with immediate
effect):

* CQL input query will no longer accept username for fields like user, creator,
  mention, watcher and contributor but instead accept AccountID, Full name
  (depending on user’s profile visibility) or public name

* Order of results coming from this API might differ from earlier due to some
  changes in search logic

With these changes, we have also introduced a new API for user search
(`/wiki/rest/api/search/user`) that you can start using today. If you have any
questions, please contact Atlassian dev support.
