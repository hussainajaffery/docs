---
title: "Frameworks and tools"
platform: cloud
product: confcloud
category: devguide
subcategory: intro
date: "2017-10-19"
---
{{% include path="docs/content/cloud/connect/concepts/jira-and-confluence-connect-frameworks.snippet.md" %}}

## Developer tools

[Confluence web fragment finder](https://marketplace.atlassian.com/plugins/com.wittified.webfragment-finder-confluence)  
The web fragment finder is an app which loads a *Web Fragment*: Web Item, Web Section, Web Panel, 
in all available Confluence locations. Web Fragments contain a unique location, making it easier to 
identify the right extension points for your app. 
 
[Connect inspector](https://connect-inspector.prod.public.atl-paas.net/)  
The Connect inspector is an extremely useful tool allowing developers to watch live lifecycle and 
webhook events in your web browser. The inspector allows you to generate a temporary Atlassian 
Connect app you install into your cloud development environment. It will live for three days and 
store any lifecycle and webhook events that it receives.

[JSON descriptor validator](https://atlassian-connect-validator.herokuapp.com/validate)  
This validator will check that your descriptor is syntactically correct. Paste the JSON content of 
your descriptor in the descriptor field, and select the Atlassian product you want to validate. 	

[JWT decoder](http://jwt-decoder.herokuapp.com/jwt/decode)  
An encoded JWT token can be opaque. Use this handy tool to decode JWT tokens and inspect their 
content. Just paste the full URL of the resource you are trying to access in the URL field, 
including the JWT token. For example:
   
``` text
https://example.atlassian.net/path/to/rest/endpoint?jwt=token
```