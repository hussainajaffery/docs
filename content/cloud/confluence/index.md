---
title: "Latest updates"
platform: cloud
product: confcloud
category: devguide
subcategory: index
date: "2019-07-17"
---

# Latest updates

We deploy updates to Confluence Cloud frequently. As a Confluence developer, it's important that
you're aware of the changes. The resources below will help you keep track of what's happening.

{{% warning %}}
#### Important! Deprecation notice for API changes to improve user privacy

On 1 October 2018 we announced that several Confluence Cloud REST APIs are being deprecated in order to improve user privacy.
Before continuing, please read the <a href="/cloud/confluence/deprecation-notice-user-privacy-api-migration-guide/">deprecation notice and migration guide</a>.
{{% /warning %}}

## Recent announcements

- [Changes to Confluence Cloud Search APIs and deprecation notice](/cloud/confluence/deprecation-notice-search-api/)
- [Deprecation notice and migration guide for major changes to Cloud Cloud REST APIs to improve user privacy](/cloud/confluence/deprecation-notice-user-privacy-api-migration-guide/)
- [Deprecation notice - Basic authentication with passwords](/cloud/confluence/deprecation-notice-basic-auth)
- [Major changes to Confluence Cloud REST APIs are coming to improve user privacy](/cloud/confluence/api-changes-for-user-privacy-announcement)
- Confluence Cloud has a new look and feel, including updated navigation. [Learn more about the new navigation](https://confluence.atlassian.com/confcloud/new-navigation-and-a-new-look-for-confluence-881536766.html).


## Atlassian Developer blog

Major changes that affect Confluence Cloud developers are announced in the
[Atlassian Developer blog](/blog/categories/confluence/), like new Confluence modules or the deprecation of API end points. You'll also find handy tips and articles related to Confluence development.

## Product updates

Major changes that affect all users of Confluence Cloud are announced in the [What's new blog](https://confluence.atlassian.com/cloud/blog) for Atlassian Cloud. This includes new features, bug fixes, and other changes.