---
title: "Introduction to Confluence Connect"
platform: cloud
product: confcloud
category: devguide
subcategory: learning
guides: tutorials
date: "2019-01-31"
aliases:
- confcloud/introduction-to-confluence-connect-39985168.html
- /confcloud/introduction-to-confluence-connect-39985168.md
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39985168
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39985168
confluence_id: 39985168
---

# Introduction to Confluence Connect

<table>
    <colgroup>
        <col width="20%">
        <col width="80%">
    </colgroup>
	<tbody>
		<tr>
			<td><strong>Description</strong></td>
			<td>A step-by-step introduction to building a basic Confluence app and macro.</td>
			</tr>
		<tr>
			<td><strong>Level</strong></td>
			<td>Beginner</td>
		</tr>
		<tr>
			<td><strong>Estimated time</strong></td>
			<td>45 minutes</td>
		</tr>
		<tr>
			<td><strong>Example</strong></td>
			<td>N/A</td>
		</tr>
	</tbody>
</table>

These lessons will get you started writing your very first Confluence Connect app step-by-step.

{{% tip %}}Want to dive in quickly and build a macro in 10 minutes? Check out the [Getting started tutorial](/cloud/confluence/getting-started).{{% /tip %}}

## Prerequisites

Ensure you have installed all the tools you need for Confluence Connect app development, and running Confluence by going through the [getting started](/cloud/confluence/getting-started/) guide.

## Lessons

Get started quickly by building a Connect app which creates a simple general page, and background-color macro. 

<table>
<colgroup>
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
<col width="20%" />
</colgroup>
<thead>
<tr class="header">
<th><div class="tablesorter-header-inner">
Title
</div></th>
<th><div class="tablesorter-header-inner">
Description
</div></th>
<th><div class="tablesorter-header-inner">
Estimated time
</div></th>
<th><div class="tablesorter-header-inner">
Example
</div></th>
<th><div class="tablesorter-header-inner">
Level
</div></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><a href="/cloud/confluence/lesson-1-the-source-awakens">Lesson 1 - The Source Awakens</a></td>
<td>A guide to setting up a general page using ACE.</td>
<td>15 minutes</td>
<td>N/A</td>
<td><div class="content-wrapper">
BEGINNER
</div></td>
</tr>
<tr class="even">
<td><a href="/cloud/confluence/lesson-2-rise-of-the-macros">Lesson 2 - Rise of the Macros</a></td>
<td>A guide to adding a Confluence macro with ACE.</td>
<td>30 minutes</td>
<td><a href="https://bitbucket.org/mjensen/example-static-macro" class="uri" class="external-link">https://bitbucket.org/mjensen/example-static-macro</a></td>
<td><div class="content-wrapper">
BEGINNER
</div></td>
</tr>
</tbody>
</table>

### Further reading

-   [Confluence REST API](/cloud/confluence/rest/)
-   [Confluence Connect patterns](/cloud/confluence/confluence-connect-patterns)