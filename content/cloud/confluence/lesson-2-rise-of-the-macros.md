---
title: "Lesson 2 - Rise of the macros"
platform: cloud
product: confcloud
category: devguide
subcategory: learning
guides: tutorials
date: "2019-04-17"
aliases:
- confcloud/lesson-2-rise-of-the-macros-39987235.html
- /confcloud/lesson-2-rise-of-the-macros-39987235.md
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39987235
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39987235
confluence_id: 39987235
---

# Lesson 2 - Rise of the macros

<table>
    <colgroup>
        <col width="20%">
        <col width="80%">
    </colgroup>
    <tbody>
        <tr>
            <td><strong>Description</strong></td>
            <td>A guide to adding a Confluence macro with ACE.</td>
        </tr>
        <tr>
            <td><strong>Level</strong></td>
            <td>Beginner</td>
        </tr>
        <tr>
            <td><strong>Estimated time</strong></td>
            <td>30 minutes</td>
        </tr>
        <tr>
            <td><strong>Example<strong></td>
            <td><a href="https://bitbucket.org/atlassianlabs/confluence_cloud_tutorials/src/rise-of-macros/" class="uri" class="external-link">https://bitbucket.org/atlassianlabs/confluence_cloud_tutorials/src/rise-of-macros/</a></td>
        </tr>
    </tbody>
</table>

## Prerequisites

This lesson follows on from [Lesson 1 - The Source Awakens](/cloud/confluence/lesson-1-the-source-awakens).

## Macros - why do we need them?

Since they are so *powerful*, macros are one of the most commonly used features of Confluence. This follows on from the fact that users of Confluence spend most of their time creating and editing content &mdash; macros are central to this experience.

This tutorial itself is a collection of macros, streamlined into a page!

## Specifying a macro module

Let's work on making a basic macro, which, when activated, allows the user to specify a background color for all enclosed content. We will build this as a static content macro. 

Before continuing, let's clarify the difference between a static and dynamic content macro.

-   **Static:** A [static content macro](/cloud/confluence/modules/static-content-macro/) is loaded synchronously into a page. This is the optimal choice for when you are building macros which will not render large amounts of content.
-   **Dynamic:** Loaded asynchronously into the page, and renders an iframe. As opposed to the static macro use case, we build [dynamic macros](/cloud/confluence/modules/dynamic-content-macro/) when large amounts of content need to be rendered.

{{% tip %}}For more information on the rendering lifecycle of these macros, check out [Matthew Jensen's AtlasCamp talk](https://www.slideshare.net/GoAtlassian/atlascamp-2014-writing-connect-addons-for-confluence).{{% /tip %}}

We specify a static content macro below. Replace the `modules` field in your app descriptor with the following:

**atlassian-connect.json**

``` bash
"modules": {
    "staticContentMacros": [{
        "url": "/v3/backgroundColor?macroId={macro.id}&color={color}&userKey={user.key}&pageId={page.id}&pageVersion={page.version}",
        "description": {
            "value": "Allow users to add a background color around selected content."
        },
        "documentation": {
            "url" : "http://www.google.com"
        },
        "categories": ["formatting"],
        "outputType": "inline",
        "bodyType": "rich-text",
        "name": {
            "value": "Background Colour (Connect)"
        },
        "key": "bg-color-macro",
        "parameters": [{
            "identifier": "color",
            "name": {
                "value": "Color"
            },
            "type": "string",
            "required": true,
            "multiple": false
        }]
    }]
}
```

Once you have saved your descriptor, reload your browser, and create a new Confluence page. 

Now for the moment of truth &mdash; type **/back**. You should have results as shown below.

![open macro browser](/cloud/confluence/images/macro-open.png)

If we select it, the Confluence macro browser appears:

![Confluence macro browser](/cloud/confluence/images/background-color-macro.png)

Awesome! How quick was that? With just a bit more code, we'll have a fully functional macro!

## Setting up a preview message

Let's now add two things:

- A route on our app server to receive the parameters inputted by a user.
- A view to be returned every time this route is hit.

Add this handler to **routes/index.js**:

``` javascript
 // Render the background-color macro.
app.get('/v3/backgroundColor', addon.authenticate(), function(req, res){

    //  Tell the app to send back the 'background-color' view as
    //  a HTTP response.
    res.render('background-color', {});
    
});
```

Then, create a new file **views/background-color.hbs** with this view:

``` html
{{!< layout}}
{{!--
    If we have received a macro body, render it.
    Otherwise, show a preview screen/message.
--}}
<div style="background-color: {{{color}}};">
    {{#if body}}
        {{{body}}}
    {{else}}
        Here is a preview of your screen!
    {{/if}}
</div>
```
 
Our macro should now display *Here is a preview of your screen!*, like this:

![edit macro](/cloud/confluence/images/edit-macro.png)

Next, let's make the coloring mechanism work. Our final step will be applying this to the macro body. 

## Setting a color

In order to grab the color inputted by a user, we update our handler to:

**routes/index.js**

``` javascript
// Render the background-color macro.
app.get('/v3/backgroundColor', addon.authenticate(), function(req, res){

    //  Grab all input parameters - sent through to us as query params.
    var color = req.query['color'];

    //  Tell the app to send back the 'background-color' view as
    //  a HTTP response.
    res.render('background-color', {
        color: color
    });

});
```

And...that's it! But how does it work? Looking at our descriptor:

1.  The color inputted by a user is sent to the app server with identifier `color`. This is the same as the parameter's `identifier` in the descriptor.
1.  This parameter is attached to our URL under the query parameter, `color`. For example, if we chose the color *red* as above, the URL being hit in our app would be */v3/backgroundColor?**color=red***.

Finally, in our index.js handler, we extract this parameter, and pass it to our template. 

## But, where's the body?

Up until this stage, we have achieved the following things:

1.  Set up a static content macro, visible in the macro browser.
1.  Prompt the user for parameter values for this macro (color).
1.  Display 'Here's a preview of your screen!' both when previewing, and actually rendering the macro on page save.
1.  Displaying this message enclosed in the background color specified by us (yay!).

To make this macro fully functional, we now need to make it a little smarter. We are easily handling the case where our macro does NOT have a body. This however, is only true for the state of the macro on initial render. Once we have committed the contents of the macro to our Confluence page, our preview message is not required. Let's now discuss how to actually get the body. The keyword here is 'GET'. 

First, we define a function `getHTTPClient()`. This function wraps a method native to ACE, namely, `addon.httpClient`. It is defined as the following, and allows your app to execute REST methods complying to your host product's REST API methods.

**routes/index.js**

``` javascript
// Returns a HTTP client which can make calls to our host product.
// @param clientKey formed when app created.
// @param userKey formed when app created.
// @returns {*} http client

function getHTTPClient (clientKey, userKey){
    return addon.httpClient({
        clientKey : clientKey,
        userKey   : userKey,
        appKey    : addon.key
    });
}
```

In order to actually get the body of our macro, we need to know which page we were working on as well as its version, and a hash of the latest macro contents. This allows us to execute a REST call to the ["Get macro body by macro ID" resource](/cloud/confluence/rest/#api-content-id-history-version-macro-id-macroId-get). We can add `pageId`, `pageVersion` and `macroId` to our query parameters by changing our app URL, like so:    

  **atlassian-connect.json**

  ``` bash
  "staticContentMacros": [{
      //...More stuff above.
      "url": "/v3/backgroundColor?macroId={macro.id}&color={color}&pageId={page.id}&pageVersion={page.version}",
      //...More stuff below.
  }]
  ```

Lastly, change the handler to the following:

**routes/index.js**

``` javascript
// Render the background-color macro.
app.get('/v3/backgroundColor', addon.authenticate(), function(req, res){

    //  Grab all input parameters - sent through to us as query params.
    var color       = req.query['color'],
        pageId      = req.query['pageId'],
        pageVersion = req.query['pageVersion'],
        macroId     = req.query['macroId'],
        userKey     = req.query['userKey'];
    var clientKey = req.context.clientKey;

    //  Execute API request to get the macro body.
    getHTTPClient(clientKey, userKey).get(
        '/rest/api/content/' + pageId +
        '/history/' + pageVersion +
        '/macro/id/' + macroId,
        function(err, response, contents){

            //  If we've encountered errors, render an error screen.
            if(err || (response.statusCode < 200 || response.statusCode > 299)) {
                console.log(err);
                res.render('<strong>An error has occurred :( '+ response.statusCode +'</strong>');
            }

            //  Parse the response, and send the body through.
            contents = JSON.parse(contents);

            //  Render with required body.
            res.render('background-color', { body : contents.body, color: color });

        }
    );

});
```

Notice we have introduced a `userKey` and `clientKey`. We grab these credentials separately, without explicitly defining a query parameter. They are exposed through the `addon.authenticate()` call we do in our route.

## Conclusion

Voila, a macro of our own making! As you can see, building a macro which can query a host product (and potentially external services) is pretty easy with ACE.