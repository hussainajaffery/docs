---
title: "Lesson 4 - Noting down information"
platform: cloud
product: confcloud
category: devguide
subcategory: learning
guides: tutorials
date: "2019-01-21"
aliases:
- confcloud/lesson-4-noting-down-information-40511961.html
- /confcloud/lesson-4-noting-down-information-40511961.md
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=40511961
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=40511961
confluence_id: 40511961
---

# Lesson 4 - Noting down information

<table>
    <colgroup>
        <col width="20%">
        <col width="80%">
    </colgroup>
    <tbody>
        <tr>
        <td><strong>Description</strong></td>
        <td>Setup and create instances of a new Note content type, contained within 'Customers'.</td>
    </tr>
    <tr>
        <td><strong>Level</strong></td>
        <td><p>Intermediate</p></td>
    </tr>
    <tr>
        <td><strong>Estimated time</strong></td>
        <td>20 minutes</td>
    </tr>
    <tr>
        <td><strong>Example</strong></td>
        <td><a href="https://bitbucket.org/atlassianlabs/confluence_cloud_tutorials/src/custom-content/" class="uri" class="external-link">https://bitbucket.org/atlassianlabs/confluence_cloud_tutorials/src/custom-content/</a></td>
    </tr>
    </tbody>
</table>

## Prerequisites

Ensure you have worked through [Lesson 3 - Extra Searching Capabilities](/cloud/confluence/lesson-3-extra-searching-capabilities) in the Custom content series.

## Another custom content type

We will now declare another custom content type in our descriptor, just like we did with customers. Let's call them 'notes'.

**atlassian-connect.json**
``` javascript
"customContent": [
  // Add this under the "Customer" type
  {
    "key": "note",
    "name": {
      "value": "Notes"
    },
    "uiSupport": {
      "contentViewComponent": {
        "moduleKey": "customersViewer"
      },
      "listViewComponent": {
        "moduleKey": "notesViewer"
        },
      "icons": {
        "item": {
          "url": "/images/notes.png"
        }
      }
    },
    "apiSupport": {
      "supportedContainerTypes": [
        "ac:my-app:customer"
      ]
    }
  }
]
```

We also add more information to our customer custom content type, as follows:

**atlassian-connect.json**
``` javascript
"customContent": [
  {
    "key": "customer",
    //  More options above here...
    "apiSupport": {
      "supportedContainerTypes": [
        "space"
      ],
      "supportedChildTypes" : [
        "ac:my-app:note"
      ]
    }   
  }
]
```

We have introduced a new key here, namely, `supportedChildTypes`. This establishes a heirarchy where a 'Note' can be contained within a 'Customer', and that a 'Note' is a supported child type of a 'Customer'. 

Just like in [Lesson 2](/cloud/confluence/lesson-2-adding-content-customers-ahoy/), we will create a note from a dialog box. Let's add another `dialog` module to our descriptor.

**atlassian-connect.json**
```javascript
"dialogs": [
  // Add this under the new customer dialog
  {
    "key": "newNote",
    "url": "/add-new-note?spaceKey={space.key}&contentId={content.id}",
    "options": {
      "height": "420px",
      "width": "600px",
      "header": {
          "value": "Add a new note"
      }
    }
  }
]
```

Here, we see space key and content ID as the URL parameters. These are just a few of the [context variables](/cloud/confluence/context-parameters/) that Confluence supports. We need these to make an API request to create notes.

Once we have added the above, we need to create a route for `/add-new-note` to render the dialog box.

**routes/index.js**
```javascript
app.get('/add-new-note', addon.authenticate(), function (req, res) {
    var contentId = req.query['contentId'];
    var spaceKey = req.query['spaceKey']
    res.render('new-note',{
        contentId: contentId,
        spaceKey: spaceKey 
    });
});
```

In terms of understanding the backend code needed, we are all done! Let's quickly look at how to create a note and retrieve all notes under a customer.

## Noteworthy content

Using `AP.request`, we perform the following:

**views/new-note.hbs** 

``` javascript
{{!< layout}}

var jsonData = {
  "type": "ac:my-app:note",
  "space": {
    "key": "{{spaceKey}}"
  },
  "container": {
    "type": "ac:my-app:customer",
    "id": "{{contentId}}"
  },
  "title": "Hello, World",
  "body": {
    "storage": {
      "value": "Goodbye, boy",
      "representation": "storage"
    }
  }
};

AP.request({
  url: '/rest/api/content',
  type: 'POST',
  contentType: 'application/json',
  data: JSON.stringify(jsonData),
  success: function(note){
    note = JSON.parse(note);
    console.log("Note successfully persisted to Confluence", note);
  },
  error: function(err){
    console.log(err);
  }
});
```

Let's pretend our customer has an id of **156831**. To see our note as a child of our customer, we issue a GET request to the **/rest/api/content/156831/child/ac:my-app:note** endpoint. We should receive a response like this: 

``` javascript
{
  "results": [
    {
      "id": "1605728",
      "title": "Hello, World",
      // More properties...
    }
  ],
  // More below here...
}
```

And, voila! A connection has been established between the customer, and the note underneath them. As you can see, this affords great possibilities to app developers in the Confluence Ecosystem. Custom content well and truly provides a fully-integrated content solution.

To wrap up, we now have all the essentials we need to build the UI we want in Confluence! Here's another snapshot of what 'notes' could look like in Confluence:

![essentials to build the UI you want in Confluence](/cloud/confluence/images/image2016-7-20-17-51-1.png)  


This concludes our deep-dive into the capabilities of Custom content in Confluence Connect.

Happy dev-ing!
