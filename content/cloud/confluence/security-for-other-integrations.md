---
title: "Security for other integrations"
platform: cloud
product: confcloud
category: devguide
subcategory: securityother
date: "2019-09-06"
---
# Security for other integrations

Integrations with Confluence Cloud can be implemented by allowing external applications or services, scripts, or even bots, to access Confluence's APIs directly. If you are not doing this and you are using the Connect framework instead, see [Security for Connect apps](../security-for-connect-apps).

Authentication and authorization are the two key parts of implementing security. Authentication tells Confluence Cloud the identity of your integration, and authorization determines what actions it can take within Confluence. If you are building an integration that does not use Atlassian Connect, there are two methods that you can use for authentication and authorization: 

*   **OAuth 2.0 authorization code grants (3LO)**: This is the recommended method. You should use this for most cases.
*   **Basic authentication**: You should only use this method for tools like personal scripts or bots.

## OAuth 2.0 authorization code grants (3LO)

OAuth 2.0 authorization code grants, also known as three-legged OAuth, is a token-based method for authentication and authorization. This method allows your integration to access Confluence APIs on a user's behalf. Unlike two-legged OAuth, three-legged OAuth involves the end user in the authorization process by asking them to consent to access to their data.

Authenticated access is provided by an access token that is used to make calls to the API. OAuth 2.0 authorization code grants require an app created via [App management](/apps/). This app provides the authentication credentials that are used to get an access token for the desired API.

Authorization for OAuth 2.0 authorization code grants is implemented via scopes. Scopes statically specify the maximum set of actions that an app may perform (see [scopes](../scopes/)). Note, OAuth 2.0 authorization code-only scopes are different to Connect scopes.

To learn more, read [OAuth 2.0 authorization code grants (3LO)](../oauth-2-authorization-code-grants-3lo-for-apps/).

## Basic authentication

Basic authentication uses an API token to authenticate the client. The token is generated from a user's Atlassian Account, encoded, then added to the header for requests to the API.

Authorization for basic authentication is based on the permissions of the user you used to generate the API token. For example, if you are using basic authentication, your user must have the _Admin_ permission for a space in order to call `PUT /wiki/rest/api/space/{spaceKey}` (Update space).

We recommend that you don't use basic authentication. It may be easier to implement, but it is much less secure. Consider using OAuth 2.0 authorization code grants (3LO) instead.

To learn more, read [Basic auth for REST APIs](../basic-auth-for-rest-apis/).