---
title: "Security overview"
platform: cloud
product: confcloud
category: devguide
subcategory: intro
date: "2019-09-06"
---
# Security overview

Security is an essential part of Confluence Cloud integrations. It lets Atlassian applications protect customer data from unauthorized access and from malicious or accidental changes. It also allows administrators to install apps with confidence, letting users enjoy the benefits of apps in a secure manner.

The methods for implementing security (that is, authentication and authorization) are different, depending on whether you are building an Atlassian Connect app or another type of integration.

## Security for Atlassian Connect apps

Atlassian Connect is an app framework used to build Atlassian apps. Atlassian Connect apps are used to extend Atlassian Cloud products, like Confluence Cloud, and can extend the user interface, access the APIs, etc. Apps built using Connect use JWT for authentication and either scopes or user impersonation for authorization.

To learn more, read [Security for Connect apps](../security-for-connect-apps).

## Security for other integrations

Connect is not the only solution for integrating with Confluence Cloud. External applications or services, scripts, or even bots, can access Confluence's APIs directly. We recommend using OAuth 2.0 authorization code grants (3LO) for these types of integrations, although basic authentication is also available. 

To learn more, read [Security for other integrations](../security-for-other-integrations).