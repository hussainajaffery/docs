---
title: "Storing data with entity properties"
product: confcloud
category: devguide
subcategory: learning
date: "2019-08-19"
---

# Storing data with entity properties

Apps can store data in the form of entity properties in Confluence. Entity properties are 
key-value pairs where the *key* is a string used to identify the property in all operations, and the 
*value* is a JSON blob. Confluence allows properties to be stored on Confluence pages with 
*content properties* or on the app itself with *app properties*. 
Your app will need to request the right [scopes](/cloud/confluence/scopes) to perform operations 
on entity properties.

Hosted data storage is useful to Atlassian Connect developers for the following reasons:

 * **Your app does not need to include a database to store data.**  
   Your app could be written as a set of static web pages using only HTML, CSS, and JavaScript, 
   without any need for an application server. Your data will be stored against the host application 
   entities.
 * **Imports and exports are handled by the host product.**  
   Since your data is stored with the host application it is included in the host applications 
   backups. This means that the import process will restore your data automatically. With entity 
   properties you never need to worry about your data being lost or disconnected from the customer.
 * **Conditions can be predicated on entity properties.**    
   You can configure whether a web fragment will be shown based on the value of an entity property.
 * **The products have access to your properties.**  
   This means that you can write CQL queries based on content properties. This 
   enables your users to enjoy the power of CQL on search data that you have defined. 
   
For more information, read [Confluence entity properties](/cloud/confluence/confluence-content-properties/).

## Query content properties with CQL

You can [use CQL to query content properties](/cloud/confluence/confluence-entity-properties/#cql-search-extension)
in your Confluence instance. For example, an app could store the number of likes on a page in a content property, then
by creating an index schema module for that property you can query the values to see how many pieces of content have
more than 20 likes. 