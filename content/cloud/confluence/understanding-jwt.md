---
title: "Understanding JWT"
platform: cloud
product: confcloud
category: devguide
subcategory: securityconnect
date: "2019-08-19"
---

{{% include path="docs/content/cloud/connect/concepts/understanding-jwt.snippet.md" %}}
