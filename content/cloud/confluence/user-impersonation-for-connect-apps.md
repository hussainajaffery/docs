---
title: "OAuth 2.0 bearer tokens for apps"
platform: cloud
product: confcloud
category: devguide
subcategory: securityconnect
date: "2019-08-19"
---
{{% include path="docs/content/cloud/connect/concepts/oauth2-jwt-bearer-token-authentication.snippet.md" %}}
