# Manage modules dynamically

## Introduction

In addition to declaring modules in the [app descriptor](../app-descriptor), Connect apps can 
register certain types of modules dynamically at runtime. The ability to dynamically register 
modules provides for flexibility in defining an app's behavior and is useful for creating
highly configurable apps that extend Jira in response to arbitrary user needs.

Modules are registered dynamically in the context of an app installation, which means that an app 
can register different modules for each Jira tenant. By contrast, an app's statically registered 
modules are the same in all tenancies.

Apps manage modules for a tenant dynamically by sending JWT-signed requests to a REST resource to:
* [register modules](../../platform/rest/v2/#api-rest-atlassian-connect-1-app-module-dynamic-post).
* [retrieve all registered modules](../../platform/rest/v2/#api-rest-atlassian-connect-1-app-module-dynamic-get).
* [remove registered modules](../../platform/rest/v2/#api-rest-atlassian-connect-1-app-module-dynamic-delete).

## Lifecycle

During module registration, the app's [translation files](https://developer.atlassian.com/cloud/jira/platform/internationalization/#translation-files) 
are available and used to resolve any [i18n](/cloud/jira/platform/internationalization) keys.

If an app is uninstalled and installed again, all of its registered modules are restored.

If a new [version](/platform/marketplace/upgrading-and-versioning-cloud-apps) of the app defines
modules statically that conflict with dynamically registered modules, _all_ the app's dynamically 
registered modules are removed. This action enables the new version to be installed. An example of 
a conflict between a dynamically and statically registered module is where they use the same key. 

## Restrictions

Up to 10 modules can be registered dynamically in a tenancy.

The initial release provides for these module types to be registered dynamically:
* [Entity property](../modules/entity-property)
* [Issue field](../modules/issue-field)


## Register modules dynamically

To register modules dynamically, call `POST /rest/atlassian-connect/1/app/module/dynamic` sending a 
JSON object containing the modules to register. The object's format is the same
as the value of the `modules` property of the [app descriptor](../app-descriptor). For example:

```json
{
    "jiraIssueFields": [
      {
        "key": "dynamic-custom-field",
        "type": "number",
        "name": {
          "value": "Custom number field"
        },
        "description": {
          "value": "This is a dynamically registered custom number field"
        }
      }
    ]
}
``` 

This object registers a number [issue field](../modules/issue-field) named _Custom number field_.

If one of the module definitions in a request has the same key as a statically or dynamically 
registered module or is invalid, none of the modules are registered. A module is invalid when it:

- is missing required properties.
- contains invalid property values. For example, numbers out of range or a string outside an allowed set.
- is not compatible with the app configuration. For example, webhook modules require the app to have the "READ" scope.

See [Register modules](../../platform/rest/v2/#api-rest-atlassian-connect-1-app-module-dynamic-post) for more information.

## Retrieve dynamically registered modules

Call `GET /rest/atlassian-connect/1/app/module/dynamic` to obtain a list of the calling app's 
registered modules. For example, for the module registered above this call returns:

```json
{
    "jiraIssueFields": [
      {
        "key": "dynamic-custom-field",
        "type": "number",
        "name": {
          "value": "Custom number field"
        },
        "description": {
          "value": "This is a dynamically registered custom number field"
        }
      }
    ]
}
``` 

See [Get modules](../../platform/rest/v2/#api-rest-atlassian-connect-1-app-module-dynamic-get) for more information.

## Remove dynamically registered modules

To remove all of an app's dynamically registered modules, call `DELETE /rest/atlassian-connect/1/app/module/dynamic`. 
To remove a module, define the key in the `moduleKey` query parameter. You can specify the parameter multiple times to remove more than one module, for example:

`DELETE /rest/atlassian-connect/1/app/module/dynamic?moduleKey=module1&moduleKey=module2`

See [Remove modules](../../platform/rest/v2/#api-rest-atlassian-connect-1-app-module-dynamic-delete) for more information.