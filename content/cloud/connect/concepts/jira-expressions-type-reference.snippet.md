# Jira Expressions Type Reference

Below you will find all types supported in [Jira expressions](../jira-expressions) along with their properties and methods.

You can examine the type of any expression at runtime with the `typeof` operator. For example, `typeof issue` will return 
`"Issue"`, which is the name of the type of the `issue` context variable. 

## Issue

* `id`: The issue ID ([Number](#number)).
* `key`: The issue key ([String](#string)).
* `summary`: The issue key ([String](#string)).
* `parent`: The issue parent ([Issue](#issue)).
* `subtasks`: The list of subtasks ([List](#list)<[Issue](#issue)>).
* `description`: The issue description ([RichText](#richtext)).
* `environment`: The value of the environment field ([RichText](#richtext)).
* `project`: The project the issue belongs to ([Project](#project)).
* `priority`: The priority of this issue ([IssuePriority](#issuepriority)).
* `assignee`: The assignee of this issue ([User](#user)).
* `reporter`: The reporter of this issue ([User](#user)).
* `creator`: The user who created this issue ([User](#user)).
* `issueType`: The issue type ([IssueType](#issuetype)).
* `status`: The issue status ([IssueStatus](#issuestatus)).
* `created`: The creation time of the issue ([Date](#date)).
* `updated`: The time when the issue was last updated at ([Date](#date)).
* `dueDate`: The time the issue is due ([CalendarDate](#calendardate)).
* `resolutionDate`: The time the issue was resolved at ([Date](#date)).
* `resolution`: The resolution of the issue ([Resolution](#resolution)).
* `originalEstimate`: The original estimate of how long working on this issue would take, in seconds ([Number](#number)).
* `remainingEstimate`: The estimate of how much longer working on this issue will take, in seconds ([Number](#number)).
* `timeSpent`: The time that was spent working on this issue, in seconds ([Number](#number)).
* `labels`: The list of labels associated with the issue ([List](#list)<[String](#string)>).
* `versions`: The list of versions the issue affects ([List](#list)<[Version](#version)>).
* `fixVersions`: The list of versions where the issue was fixed ([List](#list)<[Version](#version)>).
* `components`: The list of project components the issue belongs to ([List](#list)<[Component](#component)>).
* `comments`: The list of issue comments ([List](#list)<[Comment](#comment)>).
* `attachments`: The list of issue attachments ([List](#list)<[Attachment](#attachment)>).
* `properties`: The properties of the issue ([EntityProperties](#entityproperties)).
* `links`: The list of issue links ([List](#list)<[IssueLink](#issuelink)>).

Apart from these, all custom fields are available and can be referenced by one of the following:

* ID: `issue.customfield_10010`
* key: `issue['com.my.app.field-key']`

Custom fields are returned as JSON values, using exactly the same format as in the issue REST API.

Any field that is hidden in the field configuration active for the current issue will be returned as *null*, regardless of its actual value.
Read more about hiding and showing fields in the documentation on the [Changing a field configuration](https://confluence.atlassian.com/adminjiracloud/changing-a-field-configuration-844500798.html) page.

### Jira Software Fields

If [Jira Software](https://www.atlassian.com/software/jira) is licensed for the current user, the following fields will also be available.

* `epic`: The epic the issue belongs to ([Issue](#issue)).
* `isEpic`: *true* if the issue is an epic, *false* otherwise ([Boolean](#boolean)).
* `sprint`: The sprint the issue belongs to ([Sprint](#sprint)).
* `closedSprints`: The list of all past sprints the issue belonged to ([List](#list)<[Sprint](#sprint)>).
* `flagged`: *true* if the issue is flagged, *false* otherwise ([Boolean](#boolean)).

#### Epics

Epics (issues that have `issue.isEpic` equal to *true* or returned by `issue.epic`) have additional, epic-specific fields. 
These fields are *null* for regular issues.

* `name`: The epic name ([String](#string)).
* `done`: *true* if the epic is completed, *false* otherwise ([Boolean](#boolean)).
* `color`: The epic color ([String](#string): 'color_1', 'color_2', ..., 'color_9').
* `stories`: The list of all stories in the epic ([List](#list)<[Issue](#issue)>).
 
## Project

* `id`: The project ID ([Number](#number)).
* `key`: The project key ([String](#string)).
* `style`: The style of the project ([String](#string): 'next-gen', 'classic'). 
* `name`: The name of the project ([String](#string)).
* `projectTypeKey`: The key of the project type ([String](#string)).
* `properties`: The properties of the project ([EntityProperties](#entityproperties)).

## User

* `accountId`: The account ID of the user ([String](#string)).
* `displayName`: The display name of the user ([String](#string)).
* `properties`: The properties of the user ([EntityProperties](#entityproperties)).


## IssuePriority

* `id`: The priority ID ([Number](#number)).
* `name`: The name of the priority ([String](#string)).
* `description`: The priority description ([String](#string)).


## IssueStatus

* `id`: The status ID ([Number](#number)).
* `name`: The name of the status ([String](#string)).
* `description`: The status description ([String](#string)).
* `category`: The status category ([StatusCategory](#statuscategory)).


## StatusCategory

* `id`: The category ID ([Number](#number)).
* `key`: The key of the category ([String](#string)).
* `name`: The category name ([String](#string)).
* `colorName`: The color name associated with the category ([String](#string)).

## IssueType

* `id`: The ID of the issue type ([Number](#number)).
* `name`: The name of the issue type ([String](#string)).
* `description`: The description of the issue type ([String](#string)).
* `properties`: The issue type properties ([EntityProperties](#entityproperties)).
 
## Resolution

* `id`: The ID of the resolution ([Number](#number)).
* `name`: The name of the resolution ([String](#string)).
* `description`: The description of the resolution ([String](#string)).
 
## Comment

* `id`: The ID of the comment ([Number](#number)).
* `body`: ([RichText](#richtext)).
* `author`: The author of this comment ([User](#user)).
* `created`: The creation time of the comment ([Date](#date)).
* `updated`: The time when the comment was last updated at ([Date](#date)).
* `properties`: The comment properties ([EntityProperties](#entityproperties)).
 
## RichText

This object represents fields with rich text formatting.
Currently it allows to retrieve only plain text, but in the future it will also contain 
[Atlassian Document Format](/cloud/jira/platform/apis/document/structure).

* `plainText`: The plain text stored in the field ([String](#string)).

## IssueLink

* `id`: The ID of the issue link ([Number](#number)).
* `type`: ([IssueLinkType](#issuelinktype)).
* `direction`: Direction of the link, from the point of view of the issue the link was obtained for ([String](#string): 'inward', 'outward').
* `outwardIssue`: The outward issue of the link ([Issue](#issue)).
* `inwardIssue`: The inward issue of the link ([Issue](#issue)).
* `linkedIssue`: Issue the issue the link was obtained for is linked to ([Issue](#issue)).
 
## IssueLinkType

* `id`: The ID of the issue link type ([Number](#number)).
* `name`: The name of the issue link type ([String](#string)).
* `inward`: The name of the link when it points to the issue ([String](#string)).
* `outward`: The name of the link when it points out of the issue ([String](#string)).

## Version

* `id`: The version ID ([Number](#number)).
* `name`: The version name ([String](#string)).
* `description`: The version description ([String](#string)).
* `archived`: *true* if the version has been archived, *false* otherwise ([Boolean](#boolean)).
* `released`: *true* if the version has been released, *false* otherwise ([Boolean](#boolean)).
* `releaseDate`: The release date of the version ([CalendarDate](#calendardate)).

## Component

* `id`: The component ID ([Number](#number)).
* `name`: The component name ([String](#string)).

## Attachment

* `id`: The attachment ID ([Number](#number)).
* `author`: The user who created the attachment ([User](#user)).
* `filename`: The name of the attachment ([String](#string)).
* `size`: The size of the attachment, in bytes ([Number](#number)).
* `mimeType`: The media type of the attachment ([String](#string)).
* `created`: The attachment creation date ([Date](#date)).
 
## App 

Represents the [Connect app](../integrating-with-jira-cloud/#atlassian-connect) that provides or executes the expression.

* `key`: The app key.
* `properties`: The app properties ([EntityProperties](#entityproperties)).

## Sprint

* `id`: The sprint ID ([Number](#number)).
* `state`: The state of the sprint ([String](#string): 'future', 'active', 'closed').
* `name`: The sprint name ([String](#string)).
* `goal`: The sprint goal ([String](#string)).
* `startDate`: When the sprint started ([Date](#date)).
* `endDate`: When the sprint is planned to end ([Date](#date)).
* `completeDate`: The actual date of ending the sprint ([Date](#date)).
* `properties`: The sprint properties ([EntityProperties](#entityproperties)).

## Board

* `id`: The board ID ([Number](#number)).
* `hasBacklog`: *true* if this board can have a backlog, *false* otherwise ([Boolean](#boolean)). For example, classic Kanban boards don't have backlogs, and SCRUM boards do.
* `hasSprints`: *true* if this board supports sprints, *false* otherwise ([Boolean](#boolean)). For example, Kanban boards don't have sprints, and SCRUM boards do. 
* `activeSprints`: The list of all active sprints on this board  ([List](#list)<[Sprint](#sprint)>).
* `futureSprints`: The list of all planned sprints on this board  ([List](#list)<[Sprint](#sprint)>).
* `closedSprints`: The list of all completed sprints on this board  ([List](#list)<[Sprint](#sprint)>).
* `canAdminister`: *true* if the current user has permissions to administer this board, *false* otherwise ([Boolean](#boolean)).
* `properties`: The board properties ([EntityProperties](#entityproperties)).

## ServiceDesk

* `id`: The service desk ID ([Number](#number)).
* `project`: The Jira project this service desk is based on ([Project](#project)).

## CustomerRequest

* `issue`: The Jira issue this customer request is based on ([Issue](#issue)).
* `serviceDesk`: The service desk this request belongs to ([ServiceDesk](#servicedesk)).
* `currentStatus`: The current status of this request ([CustomerRequestStatus](#customerrequeststatus)).
* `requestType`: The type of this customer request ([CustomerRequestType](#customerrequesttype)).

## CustomerRequestStatus

* `name`: The name of this status ([String](#string)).
* `category`: The category of this status ([String](#string): 'new', 'indeterminate', 'done', 'undefined').
* `date`: The time when this status was set ([Date](#date)).

## CustomerRequestType

* `id`: The ID of this request type ([Number](#number)).

## Transition

Type of the `transition` context variable, available in Jira expressions used to implement 
[workflow conditions](../modules/workflow-condition) and [validators](../modules/workflow-validator). 
It contains information about the transition that is currently happening and allows to inspect 
the source and target status.

* `id`: The transition ID ([Number](#number)).
* `name`: The transition name ([String](#string)).
* `from`: The current status of the issue ([IssueStatus](#issuestatus)).
* `to`: The target status of the transition ([IssueStatus](#issuestatus)).
* `hasScreen`: *true* if there is a screen configured for this transition, *false* otherwise ([Boolean](#boolean)).

## EntityProperties

This object can be thought of as a map of all properties, indexed by their keys.

There are a few ways to interact with this map. It is possible to:

* Get the value of a property using static member access. For example, `issue.properties.myProperty`.
* Get the value of a property using computed member access. For example, `issue.properties['myProperty']`.
* Get the value of a property using the `get()` method. For example, `issue.properties.get('myProperty')`.
* Get a list of keys of all available properties using the `keys()` method. For example, `issue.properties.keys()`.

Apart from static and computed member access, the following methods are available:

* `get(string)`: Returns the property with the given key.
* `keys()`: Returns the the keys of all properties ([List](#list)<[String](#string)>).

Accessing a property that is not defined will return *null*. 
Properties that are JSON objects are returned as [Maps](#maps), other values can be returned as 
[List](#list), [Boolean](#boolean), [String](#string), or [Number](#number), depending on the actual type stored in the property.

## Date

This object is based on the [JavaScript Date API](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date).

Read more about dates and times in Jira expressions in [the general documentation](../jira-expressions#date-and-time). 

Jira expressions provide these additional methods:

* `toString()`: Returns a string in the human-readable format, according to the current user's locale and timezone ([String](#string)).
* `toCalendarDate()`: Transforms this into a calendar date, according to the current user's locale and timezone ([CalendarDate](#calendardate)).
* `toCalendarDateUTC()`: Transforms this into a calendar date in the UTC timezone ([CalendarDate](#calendardate)).
* `plusMonths(number)`: Returns a date with the given number of months added ([Date](#date)).
* `minusMonths(number)`: Returns a date with the given number of months removed ([Date](#date)).
* `plusDays(number)`: Returns a date with the given number of days added ([Date](#date)).
* `minusDays(number)`: Returns a date with the given number of days removed ([Date](#date)).
* `plusHours(number)`: Returns a date with the given number of hours added ([Date](#date)).
* `minusHours(number)`: Returns a date with the given number of hours removed ([Date](#date)).
* `plusMinutes(number)`: Returns a date with the given number of minutes added ([Date](#date)).
* `minusMinutes(number)`: Returns a date with the given number of minutes removed ([Date](#date)).

## CalendarDate

A time-zone agnostic [Date](#date) with the same set of methods, but limited only to year, month, and day. 

## List

Lists are a basic building block of Jira expressions. 
By design, the language does not support imperative constructs, 
so instead of writing loops, 
you need to employ the functional style of processing lists with lambda functions.

For example, to return the number of comments with contents longer than 100 characters, 
first map the comments to their texts, then filter them to leave only those long enough,
and finally get the length of the resulting list:
 
```javascript
issue.comments                              
     .map(c => c.body.plainText)         
     .filter(text => text.length > 100)  
     .length                             
```

The following properties and methods are available for lists: 

* `length`: Returns the number of items stored in the list ([Number](#number)).
* `map(Any => Any)`: Maps all items in the list to the result of the provided function ([List](#list)).
* `sort((Any, Any) => Number)`: Returns the list sorted by the natural ordering of elements or by the optional comparison function ([List](#list)).
* `filter(Any => Boolean)`: Leaves only items that do satisfy the given function, that is, for which the given function returns *true* ([List](#list)).
* `every(Any => Boolean)`: Checks if all elements in the list satisfy the given predicate ([Boolean](#boolean)).
* `some(Any => Boolean)`: Checks if the list contains at least one element that satisfies the given predicate ([Boolean](#boolean)).
* `includes(Any)`: Checks if the given argument is stored in the list ([Boolean](#boolean)).
* `indexOf(Any)` Returns the index of the first occurrence of the item in the list, or -1 ([Number](#number)).
* `slice(Number, Number?)`: Returns a portion of the list, with the index starting from the first argument (inclusive), 
and ending with the second one (exclusive). The second argument is optional, if not provided, all remaining elements will be returned. 
Negative numbers are allowed and mean indexes counted from the end of the list ([List](#list)).
* `flatten()`: Flattens a multi-dimensional list ([List](#list)).
* `flatMap(Any => Any)`: Maps all items in the list and flattens the result ([List](#list)).
* `reduce(Any => Any, Any?)`: Aggregates all elements of the list using the function provided in the first argument. 
The operation starts from the first element of the list, unless the initial value is provided in the optional second argument. 
If the list is empty and no initial value is given, an error will be returned. 
 
## Map

If the returned property value is a JSON object, it will be converted to a _Map_. 

* Static or dynamic member access can be used to retrieve values from a map. For example, `map.key` is the same as `map['key']`.
* Values can also be accessed using the `get()` method. For example, `map.get('key')`.
* Both of these methods will return *null* if there is no mapping for the given key.

To create a new map, write `new Map()`. Object literals are also evaluated to the _Map_ object. 
For example, `{ id: issue.id, summary: issue.summary }` will evaluate to a map with two keys: *id* and *summary*.

Apart from static and computed member access, the following methods are available for maps:

* `get(string)`: Returns the value mapped to the given key, or *null* (Any).
* `set(string, Any)`: Returns a new map that has all entries from the current map, plus the first argument mapped to the second ([Map](#map)). 
* `entries()`: Returns a list of all entries in this map, each entry returned as a two-element list of key and value ([List](#list)<[[String](#string), Any]>).

## String

Strings are based on the [JavaScript String object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String).

Currently supported properties and methods are:

* `length`: The string length ([Number](#number)).
* `trim()`: Removes whitespaces from beginning and end ([String](#string)).
* `toLowerCase()`: Returns the same string with all characters in lowercase ([String](#string)).
* `toUpperCase()`: Returns the same string with all characters in uppercase ([String](#string)).
* `split(string?)`: Splits the string with the given separator ([List](#list)<[String](#string)>).
* `replace(string, string|string=>string)`: Replaces all occurrences of the first argument with the second argument, which can also be a function that accepts the matched part ([String](#string)).
* `match(string)`: Finds all matches of the given regular expression in this string ([List](#list)<[String](#string)>).
* `includes(string)`: Returns *true* if this string contains the given string, *false* otherwise  ([Boolean](#boolean)).
* `indexOf(string)`: Returns the index of the first occurrence of the given string in this string, or -1 ([Number](#number)).
* `slice(number, number?)`: Returns a substring of this string, according to the given arguments ([String](#string)).

## Number
 
All numbers in Jira expressions are double-precision 64-bit IEEE 754 floating points. 
The usual set of mathematical operations is available. 

Strings can be cast to numbers with the `Number` function. For example:

    Number('1') + Number('2') == 3
    
Note that if a string cannot be parsed as number, the function returns *NaN* (Not a Number).

## Boolean
 
There are two boolean values: *true* and *false*. 
 
The usual set of logical operators, with behavior following the rules of classical boolean algebra,
is available:

* conjunction (for example, `a && b`),
* disjunction (for example, `a || b`),
* negation (for example, `!a`). 
