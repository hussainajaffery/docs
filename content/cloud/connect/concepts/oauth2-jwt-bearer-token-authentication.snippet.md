# User impersonation for Connect apps

Atlassian Connect supports user impersonation via the [JWT Bearer token](https://tools.ietf.org/html/rfc7523#section-2.1) authorization grant type for [OAuth 2.0](https://tools.ietf.org/html/rfc6749). This authorization method allows apps with the appropriate scope (`ACT_AS_USER`) to access resources and perform actions in Jira and Confluence on behalf of users.

Note that the JWT Bearer token authorization grant type for OAuth 2.0 is different to OAuth 2.0 authorization code grants (currently not supported for Confluence). JWT Bearer token authorization grant type for OAuth 2.0, also known as two-legged OAuth with impersonation (2LOi), can only be used in Connect apps. OAuth 2.0 authorization code grants, also known as three-legged OAuth (3LO), can be used in any apps or integrations.

## Flow for user impersonation authorization grants

The flow for accessing a user's resources works as follows:

![flow](/cloud/connect/images/connect-oauth-impersonation-flow.png)

1. Install hook fires with the <code>oauthClientId</code> and the shared secret.
1. App creates a JWT assertion with the shared secret and the `oauthClientId`, and then `POST`s it to the authorization server.
1. Authorization server returns an OAuth 2.0 access token.
1. App uses the access token to perform actions as a user.

## Request an OAuth 2.0 access token

For an app to make requests on a user's behalf, you need an OAuth 2.0 access token. The following steps describe how a token is retrieved:

1. **Admin installs the app:** This initiates the installation handshake with the `oauthClientId`
and the shared secret in the request body:
    ``` javascript
    {
        "key": "addon-key",
        "oauthClientId": "your-oauth2-client-id",
    }
    ```
1. **JWT token exchange:** The app creates an assertion, a JWT that is HMAC-SHA256 signed with the shared secret the app received during
the installation handshake, and adds the following claims in the payload:

   <table>
       <thead>
           <tr>
               <th>Attribute</th>
               <th>Type</th>
               <th>Description</th>
           </tr>
       </thead>
       <tr>
           <td><code>iss</code></td>
           <td>String</td>
           <td>The issuer of the claim. For example:
           <code>urn:atlassian:connect:clientid:{oauthClientId}</code>
           </td>
       </tr>
       <tr>
           <td><code>sub</code></td>
           <td>String</td>
           <td>The subject of the token. For example:
           <code>urn:atlassian:connect:useraccountid:{account ID of the user to run services on behalf of}</code>
           <em>Note:<em> <code>urn:atlassian:connect:userkey:{userkey of the user to run services on behalf of}</code> has been deprecated.
           </td>
       </tr>
       <tr>
           <td><code>tnt</code></td>
           <td>String</td>
           <td>The instance the app is installed on. For example:
           <code>https://{your-instance}.atlassian.net</code>.
           For a Confluence instance, add <code>/wiki</code> to the end.
           </td>
       </tr>
       <tr>
           <td><code>aud</code></td>
           <td>String</td>
           <td>The Atlassian authentication server: <code>https://auth.atlassian.io</code></td>
       </tr>
       <tr>
           <td><code>iat</code></td>
           <td>Long</td>
           <td>Issue time in seconds since the epoch UTC.
           </td>
       </tr>
       <tr>
           <td><code>exp</code></td>
           <td>Long</td>
           <td>Expiry time in seconds since the epoch UTC. Must be no later that 60 seconds in the future.</td>
       </tr>
   </table>
1. **OAuth bearer token generated:** The assertion and the payload are `POST`ed to the authorization server: `https://auth.atlassian.io/oauth2/token` <br>Example request:
  ``` javascript
  POST /oauth2/token HTTP/1.1
  Host: auth.atlassian.io
  Accept: application/json
  Content-Length: {length of the request body}
  Content-Type: application/x-www-form-urlencoded
  grant_type=urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Ajwt-bearer&scope=READ+WRITE&assertion={your-signed-jwt}
  ```

    - `grant_type` is the literal url-encoded `urn:ietf:params:oauth:grant-type:jwt-bearer`.
    - `assertion` is set to the assertion created in the previous step.
    - `scope` is space-delimited and capitalized. Tokens are only granted for scopes your app is authorized for.<br>
    If you omit the scope, the request is interpreted as a request for an access token with all the scopes your app has been granted.

1. **Requests on user's behalf:** The token endpoint validates the signatures and issues an access token.<br> Example response:

    ``` javascript
    HTTP/1.1 200 OK
    Status: 200 OK
    Content-Type: application/json; charset=utf-8
    ...
    {
        "access_token": "{your access token}",
        "expires_in": {15 minutes expressed as seconds},
        "token_type": "Bearer"
    }
    ```

## Using an access token in a request

Set the `Authorization` header to `Bearer {your access token}` and make your request:

Example request:

``` javascript
GET /rest/api/latest/myself HTTP/1.1
Host: {your-registered-instance}.atlassian.net
Accept: application/json
Authorization: Bearer {your-access-token}
```

Example response:

``` javascript
HTTP/1.1 200 OK
Status: 200 OK
Content-Type: application/json; charset=utf-8
...
{
  "key": "{key-of-user-to-run-services-on-behalf-of}",
  "displayName": "{impersonated user's display name}",
  ...
}
```

{{% note %}}
The example is from a Jira REST API resource &mdash; not every resource has these properties.
{{% /note %}}

## Rate limiting

Apps are allowed 5000 access token requests every 5 minutes for each host product the app is 
installed on. If you exceed the limit, the server returns a 429 status response and a JSON formatted 
error message.

Current usage limits are returned in the following headers for every server response:

* `X-RateLimit-Limit` - The number of requests allowed for 5 minutes (set to 5000).
* `X-RateLimit-Remaining` - The number of requests remaining before you hit the limit.
* `X-RateLimit-Reset` - Unix timestamp indicating when the limit will update. When this occurs, the `X-RateLimit-Remaining` count will reset to 5000 and the `X-RateLimit-Reset` time will reset to 5 minutes in the future.

Example response:

``` javascript
HTTP/1.1 200 OK
X-RateLimit-Limit: 5000
X-RateLimit-Remaining: 4999
X-RateLimit-Reset: 1366037820
...
```

## Token expiration

The OAuth 2.0 access token expiry time is included in the access token response (it is currently 15 minutes but this may change in future).
Write your code to anticipate the possibility that a granted token might no longer work.
We suggest tracking expiration time and requesting a new token before it expires, rather than handling a token expiration error. You should refresh tokens 30-60 seconds before the expiry, to make sure you are not trying to use expired tokens.

## Code Examples

Runnable Java and node.js sample code demonstrating the flow is available from
[this repository](https://bitbucket.org/atlassian/atlassian-oauth2-samples).

### Atlassian Connect for Express.js support

Atlassian Connect for Express.js provides the `.asUser` method to make requests on users' behalf.

```` javascript
var httpClient = addon.httpClient(req);
httpClient.asUser('userkey-of-user-to-act-as').get('/rest/api/latest/myself', function (err, res, body) {
  ...
})
````

### Atlassian Connect for Spring Boot support

Atlassian Connect for Spring Boot supports making requests as a user on behalf via the `AtlassianHostRestClients`
component. See [the readme](https://bitbucket.org/atlassian/atlassian-connect-spring-boot) for more information.

```` bash
@Autowired
private AtlassianHostRestClients atlassianHostRestClients;

public void doSomething() {
    atlassianHostRestClients.authenticatedAsHostActor().getForObject("/rest/api/example", Void.class);
````