# Storing data with entity properties

Apps can store data in the form of entity properties in the host application. Entity properties are 
key-value pairs where the *key* is a string used to identify the property in all operations, and the 
*value* is a JSON blob. Each host application allows properties to be stored on 
entities such as Jira issues or Confluence pages, or on your app itself. Your app will need to request 
the right [scopes](../scopes/) to perform operations on entity properties.

Hosted data storage is useful to Atlassian Connect developers for the following reasons:

* **Your app does not need to include a database to store data.**  
 Your app could be written as a set of static web pages using only HTML, CSS, and JavaScript, 
 without any need for an application server. Your data will be stored against the host application 
 entities.
* **Imports and exports are handled by the host product.**  
 Since your data is stored with the host application it is included in the host applications 
 backups. This means that the import process will restore your data automatically. With entity 
 properties you never need to worry about your data being lost or disconnected from the customer.
* **Conditions can be predicated on entity properties.**    
 You can configure whether a web fragment will be shown based on the value of an entity property.
* **The products have access to your properties.**  
 This means that you can write JQL queries based on issue entity properties, enabling users to enjoy 
 the power of JQL on [search data](../modules/entity-property/) that you have defined. 
   
For more information, read [Entity properties](../jira-entity-properties/).
