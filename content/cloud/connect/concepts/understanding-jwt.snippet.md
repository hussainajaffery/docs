# Understanding JWT for Connect apps

Atlassian Connect uses a technology called [JWT (JSON Web Token)](http://tools.ietf.org/html/draft-ietf-oauth-json-web-token) to authenticate apps. JSON Web Tokens (JWT) are a standard way of representing security claims between the app and the Atlassian host product. A JWT token is simply a signed JSON object which contains information which enables the receiver to authenticate the sender of the request.

The format of a JWT token is simple: ```<base64url-encoded header>.<base64url-encoded claims>.<signature>```.

- The header specifies a very small amount of information that the receiver needs in order to parse and verify the JWT token.
- The claims are a list of assertions that the issuer is making: each asserts that a particular field has a specific value.
- The signature is computed by using an algorithm such as HMAC SHA-256 plus the header and claims sections.

For more information about the structure of a JWT token, see [Manually creating a JWT](#creating-token-manually).

{{% note %}}
The Atlassian frameworks include tools for working with JWT. See [Frameworks and tools](frameworks-and-tools.md).
{{% /note %}}


<a name="jwtlib"></a>
## <a name="libraries"></a>JWT libraries

Most modern languages have JWT libraries available. We recommend you use one of these libraries
(or other JWT-compatible libraries) before trying to hand-craft the JWT token.

| Language | Library |
|--------------|-----------------|
| Java | [atlassian-jwt](https://bitbucket.org/atlassian/atlassian-jwt/) and [jsontoken](https://code.google.com/p/jsontoken/) |
| Python | [pyjwt](https://github.com/jpadilla/pyjwt) |
| Node.js | [atlassian-jwt-js](https://bitbucket.org/atlassian/atlassian-jwt-js) |
| Ruby | [atlassian-jwt-ruby](https://bitbucket.org/atlassian/atlassian-jwt-ruby) |
| PHP | [firebase php-jwt](https://github.com/firebase/php-jwt) and [luciferous jwt](https://github.com/luciferous/jwt) |
| .NET | [jwt](https://github.com/johnsheehan/jwt) |
| Haskell | [haskell-jwt](http://hackage.haskell.org/package/jwt) |

The [JWT decoder](http://jwt-decoder.herokuapp.com/jwt/decode) is a handy web based decoder for Atlassian Connect JWT tokens.

<a name='create'></a>
## <a name="creating-token"></a>Creating a JWT token

Here is an example of creating a JWT token, in Java, using atlassian-jwt and nimbus-jwt (last tested with atlassian-jwt version 1.5.3 and nimbus-jwt version 2.16):

``` javascript
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import com.atlassian.jwt.*;
import com.atlassian.jwt.core.writer.*;
import com.atlassian.jwt.httpclient.CanonicalHttpUriRequest;
import com.atlassian.jwt.writer.JwtJsonBuilder;
import com.atlassian.jwt.writer.JwtWriterFactory;

public class JWTSample {

    public String createUriWithJwt()
            throws UnsupportedEncodingException, NoSuchAlgorithmException {
        long issuedAt = System.currentTimeMillis() / 1000L;
        long expiresAt = issuedAt + 180L;
        String key = "atlassian-connect-addon"; //the key from the app descriptor
        String sharedSecret = "...";    //the sharedsecret key received
                                        //during the app installation handshake
        String method = "GET";
        String baseUrl = "https://&lt;my-dev-environment&gt;.atlassian.net/";
        String contextPath = "/";
        String apiPath = "/rest/api/latest/serverInfo";

        JwtJsonBuilder jwtBuilder = new JsonSmartJwtJsonBuilder()
                .issuedAt(issuedAt)
                .expirationTime(expiresAt)
                .issuer(key);

        CanonicalHttpUriRequest canonical = new CanonicalHttpUriRequest(method,
                apiPath, contextPath, new HashMap());
        JwtClaimsBuilder.appendHttpRequestClaims(jwtBuilder, canonical);

        JwtWriterFactory jwtWriterFactory = new NimbusJwtWriterFactory();
        String jwtbuilt = jwtBuilder.build();
        String jwtToken = jwtWriterFactory.macSigningWriter(SigningAlgorithm.HS256,
                sharedSecret).jsonToJwt(jwtbuilt);

        String apiUrl = baseUrl + apiPath + "?jwt=" + jwtToken;
        return apiUrl;
    }
}
```

<a name='decode'></a>
## <a name="decoding-and-verifying-token"></a>Decoding and verifying a JWT

Here is a minimal example of decoding and verifying a JWT token, in Java, using atlassian-jwt and nimbus-jwt (last tested with atlassian-jwt version 1.5.3 and nimbus-jwt version 2.16).

**NOTE:** This example does not include any error handling.
See [`AbstractJwtAuthenticator`](https://bitbucket.org/atlassian/atlassian-jwt/src/master/core/src/main/java/com/atlassian/jwt/core/http/auth/AbstractJwtAuthenticator.java)
from atlassian-jwt for recommendations of how to handle the different error cases.

``` javascript
import com.atlassian.jwt.*;
import com.atlassian.jwt.core.http.JavaxJwtRequestExtractor;
import com.atlassian.jwt.core.reader.*;
import com.atlassian.jwt.exception.*;
import com.atlassian.jwt.reader.*;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

public class JWTVerificationSample {

    public Jwt verifyRequest(HttpServletRequest request,
                             JwtIssuerValidator issuerValidator,
                             JwtIssuerSharedSecretService issuerSharedSecretService)
            throws UnsupportedEncodingException, NoSuchAlgorithmException,
                   JwtVerificationException, JwtIssuerLacksSharedSecretException,
                   JwtUnknownIssuerException, JwtParseException {
        JwtReaderFactory jwtReaderFactory = new NimbusJwtReaderFactory(
                issuerValidator, issuerSharedSecretService);
        JavaxJwtRequestExtractor jwtRequestExtractor = new JavaxJwtRequestExtractor();
        CanonicalHttpRequest canonicalHttpRequest
                = jwtRequestExtractor.getCanonicalHttpRequest(request);
        Map&lt;String, ? extends JwtClaimVerifier&gt; requiredClaims = JwtClaimVerifiersBuilder.build(canonicalHttpRequest);
        String jwt = jwtRequestExtractor.extractJwt(request);
        return jwtReaderFactory.getReader(jwt).readAndVerify(jwt, requiredClaims);
    }
}
```

### <a name="decoding-token"></a>Decoding a JWT token

Decoding the JWT token reverses the steps followed during the creation of the token,
to extract the header, claims and signature. Here is an example in Java:

``` javascript
String jwtToken = ...;//e.g. extracted from the request
String[] base64UrlEncodedSegments = jwtToken.split('.');
String base64UrlEncodedHeader = base64UrlEncodedSegments[0];
String base64UrlEncodedClaims = base64UrlEncodedSegments[1];
String signature = base64UrlEncodedSegments[2];
String header = base64Urldecode(base64UrlEncodedHeader);
String claims = base64Urldecode(base64UrlEncodedClaims);
```

This gives us the following:

Header:
``` javascript
{
    "alg": "HS256",
    "typ": "JWT"
}
```
Claims:
``` json
{
    "iss": "jira:15489595",
    "iat": 1386898951,
    "qsh": "8063ff4ca1e41df7bc90c8ab6d0f6207d491cf6dad7c66ea797b4614b71922e9",
    "exp": 1386899131
}
```
Signature:

``` html
uKqU9dTB6gKwG6jQCuXYAiMNdfNRw98Hw_IWuA5MaMo
```
<a name='verify'></a>
### <a name="verifying-token"></a>Verifying a JWT token

JWT libraries typically provide methods to be able to verify a received JWT token.
Here is an example using nimbus-jose-jwt and json-smart:

``` javascript
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import net.minidev.json.JSONObject;

public JWTClaimsSet read(String jwt, JWSVerifier verifier) throws ParseException, JOSEException
{
    JWSObject jwsObject = JWSObject.parse(jwt);

    if (!jwsObject.verify(verifier))
    {
        throw new IllegalArgumentException("Fraudulent JWT token: " + jwt);
    }

    JSONObject jsonPayload = jwsObject.getPayload().toJSONObject();
    return JWTClaimsSet.parse(jsonPayload);
}
```

<a name='qsh'></a>
### <a name="qsh"></a>Creating a query string hash

A query string hash is a signed canonical request for the URI of the API you want to call.

``` javascript
qsh = `sign(canonical-request)`
canonical-request = `canonical-method + '&' + canonical-URI + '&' + canonical-query-string`
```
A canonical request is a normalised representation of the URI. Here is an example. For the following URL,
assuming you want to do a "GET" operation:

``` javascript
"https://<my-dev-environment>.atlassian.net/path/to/service?zee_last=param&repeated=parameter 1&first=param&repeated=parameter 2"
```
The canonical request is

``` javascript
"GET&/path/to/service&first=param&repeated=parameter%201,parameter%202&zee_last=param"
```
To create a query string hash, follow the detailed instructions below:

1.  Compute canonical method
    -   Simply the upper-case of the method name (e.g. `"GET"` or `"PUT"`)
2.  Append the character `'&'`
3.  Compute canonical URI
    -   Discard the protocol, server, port, context path and query parameters from the full URL.
    -   For requests targeting apps discard the `baseUrl` in the app descriptor.
    -   Removing the context path allows a reverse proxy to redirect incoming requests for `"jira.example.com/getsomething"`
   to `"example.com/jira/getsomething"` without breaking authentication. The requester cannot know that the reverse proxy
   will prepend the context path `"/jira"` to the originally requested path `"/getsomething"`
    -   Empty-string is not permitted; use `"/"` instead.
    -   Url-encode any `'&'` characters in the path.
    -   Do not suffix with a `'/'` character unless it is the only character. e.g.
        -   Canonical URI of `"https://example.atlassian.net/wiki/some/path/?param=value"` is `"/some/path"`
        -   Canonical URI of `"https://example.atlassian.net"` is `"/"`
4.  Append the character `'&'`
5.  Compute canonical query string
    -   The query string will use [percent-encoding](http://en.wikipedia.org/wiki/Percent-encoding).
    -   Sort the parameters primarily by their percent-encoded names and secondarily by their percent-encoded values.
        -   Include all the parameters from query string if it is a GET request.
        -   Include all the post data in payload if it is a POST request.
    -   Sorting is by codepoint: `sort(["a", "A", "b", "B"]) => ["A", "B", "a", "b"]`
    -   For each parameter append its percent-encoded name, the `'='` character and then its percent-encoded value. If the parameter has no value, the `'='` character must still be included: `&a=&b=foo&c=`
    -   In the case of repeated parameters, append the encoded `','` character (i.e., `"%2C"`) and subsequent percent-encoded values.
    -   Ignore the `jwt` parameter, if present.
    -   Some particular values to be aware of:
        -   A whitespace character is encoded as `"%20"`,
        -   `","` as `"%2C"`,
        -   `"+"` as `"%2B"`,
        -   `"*"` as `"%2A"` and
        -   `"~"` as `"~"`.<br>
    (These values used for consistency with OAuth1.)
6.  Convert the canonical request string to bytes
    -   The encoding used to represent characters as bytes is `UTF-8`
7.  Hash the canonical request bytes using the `SHA-256` algorithm
    -   e.g. The `SHA-256` hash of `"foo"` is `"2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae"`

## <a name="creating-token-manually"></a>Manually creating a JWT

<div class="aui-message warning">
  <div class="icon"></div>
  <p>
    You should only need to read this section if you are planning to create JWT tokens manually,
    i.e. if you are not using one of the libraries listed in the previous section
  </p>
</div>

A JWT token looks like this:

``` javascript
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjEzODY4OTkxMzEsImlzcyI6ImppcmE6MTU0ODk1OTUiLCJxc2giOiI4MDYzZmY0Y2ExZTQxZGY3YmM5MGM4YWI2ZDBmNjIwN2Q0OTFjZjZkYWQ3YzY2ZWE3OTdiNDYxNGI3MTkyMmU5IiwiaWF0IjoxMzg2ODk4OTUxfQ.uKqU9dTB6gKwG6jQCuXYAiMNdfNRw98Hw_IWuA5MaMo
```
The basic format is:

``` javascript
<base64url-encoded header>.<base64url-encoded claims>.<base64url-encoded signature>
```
In other words:

* You create a header object, with the JSON format. Then you encode it in base64url
* You create a claims object, with the JSON format. Then you encode it in base64url
* You create a signature for the URI (we'll get into that later). Then you encode it in base64url
* You concatenate the three items, with the "." separator

You shouldn't actually have to do this manually, as there are libraries available in most languages, as we describe in the [JWT libraries](#jwtlib) section.

However it is important you understand the fields in the JSON header and claims objects described in the next sections:

### <a name="token-structure-header"></a>Header

The header object declares the type of the encoded object and the algorithm used for the cryptographic signature. Atlassian Connect always
uses the same values for these. The typ property will be "JWT" and the alg property will be "HS256".
``` json
{
    "typ":"JWT",
    "alg":"HS256"
}
```
<table>
    <thead>
        <tr>
            <th>Attribute</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tr>
        <td><code>typ</code></td><td>String</td><td>Type for the token, defaulted to "JWT". Specifies that this is a JWT token</td>
    </tr>
    <tr>
        <td><code>alg</code> (mandatory)</td><td>String</td><td>Algorithm. specifies the algorithm used to sign the token.
            In atlassian-connect version 1.0 we support the HMAC SHA-256 algorithm, which the
            <a href="http://tools.ietf.org/html/draft-ietf-oauth-json-web-token-13">JWT specification</a> identifies using the string <a href="http://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-18#section-3.1">HS256</a>.</td>
    </tr>
</table>

<div class="aui-message warning">
  <div class="icon"></div>
  <p>
    Your JWT library or implementation should discard any tokens which specify `alg: none` as this can provide a bypass of the token verification.
  </p>
</div>


<a name='claims'></a>
### <a name="token-structure-claims"></a>Claims

The claims object contains security information about the message you're transmitting. The attributes of this object provide information to ensure the authenticity of the claim. The information includes the issuer, when the token was issued, when the token will expire, and other
contextual information, described below.
``` json
{
    "iss": "jira:1234567",
    "iat": 1300819370,
    "exp": 1300819380,
    "qsh": "8063ff4ca1e41df7bc90c8ab6d0f6207d491cf6dad7c66ea797b4614b71922e9",
    "sub": "mia",
    "context": {
        "user": {
            "accountId": "123456:1234abcd-1234-abcd-1234-1234abcd1234",
            "userKey": "mia",
            "username": "mkrystof",
            "displayName": "Mia Krystof"
        }
    }
}
```
<table>
    <thead>
        <tr>
            <th>Attribute</th>
            <th>Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tr>
        <td><code>iss</code> (mandatory)</td>
        <td>String</td>
        <td>the issuer of the claim. Connect uses it to identify the application making the call. for example:
            <ul>
                <li>If the Atlassian product is the calling application: contains the unique identifier of the tenant.
             This is the `clientKey` that you receive in the `installed` callback. You should reject unrecognised issuers.
                <li>If the app is the calling application: the app key specified in the app descriptor
            </ul>
        </td>
    </tr>
    <tr>
        <td><code>iat</code> (mandatory)</td>
        <td>Long</td>
        <td>Issued-at time. Contains the UTC Unix time at which this token was issued. There are no hard
            requirements around this claim but it does not make sense for it to be significantly in the future.
            Also, significantly old issued-at times may indicate the replay of suspiciously old tokens. </td>
    </tr>
    <tr>
        <td><code>exp</code> (mandatory)</td>
        <td>Long</td>
        <td>Expiration time. It contains the UTC Unix time after which you should no longer accept this token.
            It should be after the issued-at time.</td>
    </tr>
    <tr>
        <td><code>qsh</code> (mandatory)</td>
        <td>String</td>
        <td>query string hash. A custom Atlassian claim that prevents URL tampering.</td>
    </tr>
    <tr>
        <td><code>sub</code> (optional)</td>
        <td>String</td>
        <td>The subject of this token. This is the user associated with the relevant action, and may not be present if there is no logged in user. This may be the user's Atlassian Account ID (added by GDPR API migration) or userKey (marked for deprecation) (removed by <a href="../connect-app-migration-guide/">GDPR API Migration</a>).</td>
    </tr>
    <tr>
        <td><code>aud</code> (optional)</td>
        <td>String or String[]</td>
        <td>The audience(s) of this token. For REST API calls from an app to a product, the audience claim can be
        used to disambiguate the intended recipients. This attribute is not used for Jira and Confluence at the moment,
        but will become mandatory when making REST calls from an app to e.g. the bitbucket.org domain.</td>
    </tr>
    <tr>
        <td><code>context</code> (optional)</td>
        <td>Object</td>
        <td>The context claim is an extension added by Atlassian Connect which may contain useful context for outbound requests (from the product to your app).
            The current user (the same user in the <code>sub</code> claim) is added to the context. This contains the account ID, user key, username, and display name for the subject. (context.user marked for deprecation.) (removed by <a href="../connect-app-migration-guide/">GDPR API Migration</a>)
            <code data-language="json" data-format="block">
"context": {
    "user": {
        "accountId": "123456:1234abcd-1234-abcd-1234-1234abcd1234",
        "userKey": "mia",
        "username": "mkrystof",
        "displayName": "Mia Krystof"
    }
}
            </code>
            <ul>
                <li><code>accountId</code> &mdash; the primary key of the user, global across Atlassian. Use the <code>accountId</code> any time you want to store a reference to a user in long term storage (e.g., a database or an index). The <code>accountId</code> can never change. The account ID should never be displayed to the user as it is not a human-readable value.</li>
                <li><code>userKey</code> &mdash; the legacy primary key of the user.</li>
                <li><code>username</code> &mdash; a unique secondary key. It should not be stored in long-term storage because it can change over time. This is the value that the user has traditionally logged into the application with.</li>
                <li><code>displayName</code> &mdash; the user's name.</li>
            </ul>
        </td>
    </tr>
</table>

You should use a little leeway when processing time-based claims, as clocks may drift apart.
The JWT specification suggests no more than a few minutes.
Judicious use of the time-based claims allows for replays within a limited window.
This can be useful when all or part of a page is refreshed or when it is valid for a user to
repeatedly perform identical actions (e.g. clicking the same button).

<a name='signature'></a>
### <a name="token-structure-signature"></a>Signature

The signature of the token is a combination of a hashing algorithm combined with the header and claims sections of the token.
This provides a way to verify that the claims and headers haven't been been compromised during transmission. The signature
will also detect if a different secret is used for signing. In the JWT spec, there are multiple algorithms you can use to create the
signature, but Atlassian Connect uses the HMAC SHA-256 algorithm. If the JWT token has no specified algorithm, you should discard that
token as they're not able to be signature verified.


### Steps to Follow

1.  Create a header JSON object
2.  Convert the header JSON object to a UTF-8 encoded string and base64url encode it. That gives you encodedHeader.
3.  Create a claims JSON object, including a [query string hash](#qsh)
4.  Convert the claims JSON object to a UTF-8 encoded string and base64url encode it. That gives you encodedClaims.
5.  Concatenate the encoded header, a period character (```.```) and the encoded claims set. That gives you signingInput = encodedHeader+ "." + encodedClaims.
6.  Compute the signature of signingInput using the JWT or cryptographic library of your choice. Then base64url encode it. That gives you encodedSignature.
7.  concatenate the signing input, another period character and the signature, which gives you the JWT token. jwtToken = signingInput + "." + encodedSignature

### Example

 Here is an example in Java using gson, commons-codec, and the Java security and crypto libraries:

``` java
public class JwtClaims {
     protected String iss;
     protected long iat;
     protected long exp;
     protected String qsh;
     protected String sub;
    // + getters/setters/constructors
}

[...]

public class JwtHeader {
    protected String alg;
    protected String typ;
     // + getters/setters/constructors
}

[...]

import static org.apache.commons.codec.binary.Base64.encodeBase64URLSafeString;
import static org.apache.commons.codec.binary.Hex.encodeHexString;
import java.io.UnsupportedEncodingException;
import java.security.*;
import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import com.google.gson.Gson;

public class JwtBuilder {

    public static String generateJWTToken(String requestUrl, String canonicalUrl,
        String key, String sharedSecret)
                 throws NoSuchAlgorithmException, UnsupportedEncodingException,
                 InvalidKeyException {

        JwtClaims claims = new JwtClaims();
        claims.setIss(key);
        claims.setIat(System.currentTimeMillis() / 1000L);
        claims.setExp(claims.getIat() + 180L);

        claims.setQsh(getQueryStringHash(canonicalUrl));
        String jwtToken = sign(claims, sharedSecret);
        return jwtToken;
    }

    private static String sign(JwtClaims claims, String sharedSecret)
            throws InvalidKeyException, NoSuchAlgorithmException {
         String signingInput = getSigningInput(claims, sharedSecret);
         String signed256 = signHmac256(signingInput, sharedSecret);
         return signingInput + "." + signed256;
     }

     private static String getSigningInput(JwtClaims claims, String sharedSecret)
            throws InvalidKeyException, NoSuchAlgorithmException {
         JwtHeader header = new JwtHeader();
         header.alg = "HS256";
         header.typ = "JWT";
         Gson gson = new Gson();
         String headerJsonString = gson.toJson(header);
         String claimsJsonString = gson.toJson(claims);
         String signingInput = encodeBase64URLSafeString(headerJsonString
                 .getBytes())
                 + "."
                 + encodeBase64URLSafeString(claimsJsonString.getBytes());
         return signingInput;
     }

     private static String signHmac256(String signingInput, String sharedSecret)
            throws NoSuchAlgorithmException, InvalidKeyException {
        SecretKey key = new SecretKeySpec(sharedSecret.getBytes(), "HmacSHA256");
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(key);
        return encodeBase64URLSafeString(mac.doFinal(signingInput.getBytes()));
    }

    private static String getQueryStringHash(String canonicalUrl)
            throws NoSuchAlgorithmException,UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
         md.update(canonicalUrl.getBytes("UTF-8"));
         byte[] digest = md.digest();
         return encodeHexString(digest);
     }
 }

[...]

public class Sample {
    public String getUrlSample() throws Exception {
        String requestUrl =
            "https://&lt;my-dev-environment&gt;.atlassian.net/rest/atlassian-connect/latest/license";
        String canonicalUrl = "GET&/rest/atlassian-connect/latest/license&";
        String key = "...";     //from the app descriptor
                            //and received during installation handshake
        String sharedSecret = "..."; //received during installation Handshake

        String jwtToken = JwtBuilder.generateJWTToken(
            requestUrl, canonicalUrl, key, sharedSecret);
        String restAPIUrl = requestUrl + "?jwt=" + jwtToken;
        return restAPIUrl;
    }
}
```
