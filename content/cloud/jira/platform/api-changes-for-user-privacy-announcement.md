---
title: "Important notice: Upcoming changes to Jira Cloud REST APIs to improve user privacy"
platform: cloud
product: jiracloud
category: devguide
subcategory: updates
date: "2018-05-25"
---

# Major changes to Jira Cloud REST APIs are coming to improve user privacy

Throughout 2018 and 2019, Atlassian will undertake a number of changes to our products and APIs
in order to improve user privacy in accordance with the [European General Data Protection
Regulation (GDPR)]. In addition to pursuing relevant certifications and data handling standards,
we will be rolling out changes to Atlassian Cloud product APIs to consolidate how personal data
about Atlassian product users is accessed by API consumers.

This page summarizes the relevant API changes that we expect to make in the future. Where
possible, we provide a link to specific Jira issues that you can track to stay up to date about
specific changes and when they will go into effect. We encourage you to watch these issues and
*check this page regularly* in order to stay up to date about any API changes.

## Related announcements

This announcement provides supplementary information to related [Major changes to Atlassian Connect APIs are coming to improve user privacy](../api-changes-for-user-privacy-announcement-connect/).

## Introduction of Atlassian account ID

User objects are returned by a number of Jira REST API endpoints. For example:

- The `/user` endpoint returns representations of Jira users
- The `/groups` endpoint can be expanded to return representations of each user in a group
- The `/issue` endpoint returns users in user-based fields like assignee, reporter, comments, and worklogs
- The `/component` resource returns user details of the component lead

For a full list of affected APIs, see the table at the bottom of this post.

{{% note %}}
In all cases where Jira APIs return user details, the object body now includes the user's Atlassian account ID
(`accountId`). The `accountId` is a unique identifier for an Atlassian account user and should be considered
the primary key for interacting with users via Atlassian APIs.
{{% /note %}}

If you store user data, we strongly encourage you to use `accountId` to identify users.

## Changes to Jira user objects

When a user object is returned by a Jira API today, it includes a number of attributes about a user,
like `emailAddress`, `displayName`, and `avatarUrl`. These user objects will change substantially
following the deprecation period. Below is a summary of changes:

|Attribute|Status|
|---|---|
|`self`|Changed to reference Atlassian account API URL.|
|`name`|Removed following the deprecation period.|
|`key`|Will be changed to return the same value as `accountId` for new users without notice and then removed following the deprecation period.|
|`accountId`|Will always be returned. Primary identifier for users.|
|`emailAddress`|Will be returned if allowed by user's privacy settings. May be null.|
|`displayName`|Value returned is determined by user's privacy settings. Will be non-null.|
|`active`|No change|
|`timeZone`|Will be returned if allowed by user's privacy settings. May be null.|
|`avatarUrls`|Current avatar URL will be removed following the deprecation period. New avatar resources will be introduced.|
|`nickname`|(New) A user-customizable "handle" to refer to a user, such as in an @mention|

Atlassian will provide a public **Atlassian account API** to access individual user details later this year. Please watch [ACJIRA-1510]
to be notified about these changes.

## Removal of username values from various Jira API resources

Currently, Jira users also have a `username` identifier, which is a mutable, per-user identifier
within a single Jira instance. As we expect API consumers to use `accountId` as the
primary identifier for users, the user name value will be removed from all locations in the
future, including as markup for mentioning users in a text field, such as `[~username]`. This will
be replaced with `[~accountId]`. Please watch [ACJIRA-1511] to be notified about these changes.

## Updates to APIs which accept user name or key as input

A number of Jira API endpoints currently accept Jira user names as path parameters, query
parameters, or in request bodies. Jira will introduce new versions for each affected API that
accepts the `username` or `userKey` parameters. In all cases, requests that previously used a
user name or user key will only accept an `accountId` in the new API version.

## Jira REST APIs changing in response to GDPR

The table below contains affected API resources and tickets to watch.

|Resource|Methods|Ticket to watch for updates|
|---|---|---|
|/api/2/user|GET, POST, DELETE|[ACJIRA-1497]|
|/api/2/user/search|GET|[ACJIRA-1497]|
|/api/2/user/assignable/search|GET|[ACJIRA-1497]|
|/api/2/user/password|PUT|Already removed as of November 2016|
|/api/2/user/viewissue/search|GET|[ACJIRA-1497]|
|/api/2/user/permission/search|GET|[ACJIRA-1497]|
|/api/2/user/assignable/multiProjectSearch|GET|[ACJIRA-1497]|
|/api/2/user/avatar|GET, POST, PUT, DELETE|Already deprecated|
|/api/2/user/avatar/temporary|GET, POST, PUT|Already deprecated|
|/api/2/user/columns|GET, POST, DELETE|[ACJIRA-1497]|
|/api/2/user/properties|GET, PUT|[ACJIRA-1497]|
|/api/2/user/properties/{propertyKey}|GET, DELETE|[ACJIRA-1497]|
|/api/2/component|POST, PUT|[ACJIRA-1498]|
|/api/2/myself|GET|[ACJIRA-1499]|
|/api/2/groups/picker|GET|[ACJIRA-1500]|
|/api/2/group/user|GET, DELETE|[ACJIRA-1500]|
|/api/2/issue/{issueIdOrKey}|PUT, POST|[ACJIRA-1501]|
|/api/2/issue/{issueIdOrKey}/watchers|POST, DELETE|[ACJIRA-1501]|
|/api/2/project/{projectIdOrKey}/role/{id}|PUT|[ACJIRA-1502]|
|/api/2/viewuser/application/{applicationKey}|POST, DELETE|Already removed as of November 2016|
|/api/2/project{projectKey}/avatar/{avatarId}|POST|[ACJIRA-1502]|
|/api/2/dashboard|GET|[ACJIRA-1503]|
|/api/2/filter|POST|[ACJIRA-1504]|
|/api/2/project{projectKey}/avatar/{avatarId}|PUT|[ACJIRA-1502]|
|/api/2/users/picker|GET|[ACJIRA-1505]|
|/rest/agile/1.0/board/{boardId}/issue|GET|[ACJIRA-1506]|
|/rest/agile/1.0/board/{boardId}/backlog|GET|[ACJIRA-1506]|
|/rest/agile/1.0/epic/{epicIdOrKey}/issue|GET|[ACJIRA-1506]|
|/rest/agile/1.0/board/{boardId}/epic/none/issue|GET|[ACJIRA-1506]|
|/rest/agile/1.0/board/{boardId}/sprint/{sprintId}/issue|GET|[ACJIRA-1506]|
|/rest/agile/1.0/issue/{issueIdOrKey}|GET|[ACJIRA-1506]|
|/rest/agile/1.0/sprint/{sprintId}/issue|GET|[ACJIRA-1506]|
|/rest/servicedeskapi/organization/{organizationId}/<br/>user|POST, DELETE|[ACJIRA-1507]|
|/rest/servicedeskapi/request/{issueIdOrKey}/<br/>participant|POST, DELETE|[ACJIRA-1507]|
|/rest/servicedeskapi/servicedesk/{serviceDeskId}/<br/>customer|POST, DELETE|[ACJIRA-1507]|
|/rest/servicedeskapi/request|POST|[ACJIRA-1507]|
|**Webhooks**| |[ACJIRA-1508]|
|**Context parameters**|user_id|[ACJIRA-1509]<br/>Already deprecated and will be removed|
| |user_key|[ACJIRA-1509]<br/>Already deprecated and will be removed|
| |profileUser.name|[ACJIRA-1509]|
| |profileUser.key|[ACJIRA-1509]|


[European General Data Protection Regulation (GDPR)]: https://www.atlassian.com/blog/announcements/atlassian-and-gdpr-our-commitment-to-data-privacy
[Atlassian REST API Policy]: /platform/marketplace/atlassian-rest-api-policy/
[ACJIRA-1497]: https://ecosystem.atlassian.net/browse/ACJIRA-1497
[ACJIRA-1498]: https://ecosystem.atlassian.net/browse/ACJIRA-1498
[ACJIRA-1499]: https://ecosystem.atlassian.net/browse/ACJIRA-1499
[ACJIRA-1500]: https://ecosystem.atlassian.net/browse/ACJIRA-1500
[ACJIRA-1501]: https://ecosystem.atlassian.net/browse/ACJIRA-1501
[ACJIRA-1502]: https://ecosystem.atlassian.net/browse/ACJIRA-1502
[ACJIRA-1503]: https://ecosystem.atlassian.net/browse/ACJIRA-1503
[ACJIRA-1504]: https://ecosystem.atlassian.net/browse/ACJIRA-1504
[ACJIRA-1505]: https://ecosystem.atlassian.net/browse/ACJIRA-1505
[ACJIRA-1506]: https://ecosystem.atlassian.net/browse/ACJIRA-1506
[ACJIRA-1507]: https://ecosystem.atlassian.net/browse/ACJIRA-1507
[ACJIRA-1508]: https://ecosystem.atlassian.net/browse/ACJIRA-1508
[ACJIRA-1509]: https://ecosystem.atlassian.net/browse/ACJIRA-1509
[ACJIRA-1510]: https://ecosystem.atlassian.net/browse/ACJIRA-1510
[ACJIRA-1511]: https://ecosystem.atlassian.net/browse/ACJIRA-1511