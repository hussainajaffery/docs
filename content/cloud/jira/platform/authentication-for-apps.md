---
title: "Authentication for Connect apps"
platform: cloud
product: jiracloud
category: devguide
subcategory: securityconnect
aliases:
    - /cloud/jira/platform/authentication-for-add-ons.html
    - /cloud/jira/platform/authentication-for-add-ons.md
date: "2019-08-19"
---
{{% include path="docs/content/cloud/connect/concepts/authentication.snippet.md" %}}