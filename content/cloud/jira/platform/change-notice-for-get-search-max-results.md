---
title: "Change notice - GET search optimized for fields id and key"
platform: cloud
product: jiracloud
category: devguide
subcategory: updates
date: "2019-03-08"
---

# Change notice - GET search optimized for fields `id` and `key`

We are in the process of optimizing [GET /rest/api/2/search](/cloud/jira/platform/rest/v2/#api-rest-api-2-search-get) so that it returns the highest achievable number of entries per page based on the fields requested.

The first optimization is for the `id` and `key` fields. Requests for only these fields enable a maximum of 1000 entries to be returned. Note, however, that this maximum is subject to change as we monitor the live performance. 

As with any maximum limit on the number of items returned to a page, we recommended you don’t make any assumptions about that limit when coding.

## Need help

If you need help with this change, ask in the
[Jira Cloud Development](https://community.developer.atlassian.com/c/Jira/jira-cloud)
forum in the [Developer Community](https://community.developer.atlassian.com/).