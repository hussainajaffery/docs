---
title: "Change notice - Get my permissions resource will require a permissions query parameter"
platform: cloud
product: jiracloud
category: devguide
subcategory: updates
date: "2018-07-16"
---

# Change notice - Get my permissions resource will require a `permissions` query parameter

In order to guarantee stability and performance of Jira Cloud, we are constantly
reviewing Jira REST APIs to make sure their design enables and encourages good
practices. Starting 1 February 2019, we will require that you provide the
`permissions` query parameter in all requests to the
[Get my permissions](https://developer.atlassian.com/cloud/jira/platform/rest/#api-rest-api-2-mypermissions-get)
resource.


## Why are we doing this

[Get my permissions](https://developer.atlassian.com/cloud/jira/platform/rest/#api-rest-api-2-mypermissions-get)
checks permissions for a user. The list of permissions is extendable and can
grow without limit. This can make requests slow, as the operation checks all
permissions by default. In addition, this method is usually only used to
examine a particular set of permissions, so checking all permissions is
redundant.

## New `permissions` query parameter

To solve this performance issue, we're introducing a new query parameter called
`permissions`. This parameter is a comma-separated list of permission keys.
Only permissions on this list will be returned in the response.

The parameter will become required and requests without it will start being
rejected after 1 February 2019.

You will still be able to request all permissions by listing them in the
permissions query parameter. You can get all permissions defined in a Jira
instance using the
[Get all permissions](https://developer.atlassian.com/cloud/jira/platform/rest/#api-rest-api-2-permissions-get)
endpoint.

## Let's see the code

### Deprecated call

The request below checks all permissions.

``` bash
https://your-domain.atlassian.net/rest/api/2/mypermissions
```

### New call

The request below checks only permissions defined in the `permissions` query parameter.

``` bash
https://your-domain.atlassian.net/rest/api/2/mypermissions?permissions=BROWSE_ISSUES,CREATE_ISSUES,ADMINISTER_PROJECTS
```

## Need help

If you need help about this change please ask in the
[Jira Cloud Development](https://community.developer.atlassian.com/c/Jira/jira-cloud)
forum in the [Developer Community](https://community.developer.atlassian.com/).
