---
title: "Change notice: Padding update for Jira Cloud navigation UI"
platform: cloud
product: jiracloud
category: devguide
subcategory: changelog
date: "2019-08-20"
---
# Change notice: Padding update for Jira Cloud navigation UI

From 22 Aug 2019, we are increasing the spacing between Jira’s navigation sidebar and vendor
applications by 20px. This is a short-term fix to ensure that our sidebar does not interfere
with how apps are build and embedded in our products.

![Jira Sidebar with 20px padding example image.](../images/jira-sidebar-update-annotated.png)

We are in the process of updating the padding of the navigation UI so that there is more 
flexibility in how third-party developers want to build their product interface. 
We’re adding 20 pixels to the sidebar to increase spacing between the Jira navigation UI
and the app UI. This change is cosmetic and should not affect functionality of third-party apps.

## Rollout

This change impacts all apps that utilize Jira’s navigation UI. We are rolling this change out 
incrementally, beginning on 22 August 2019 and will be fully released by the end of August 2019.