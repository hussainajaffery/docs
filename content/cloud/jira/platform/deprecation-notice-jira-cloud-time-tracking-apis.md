---
title: "Deprecation Notice: Removal of the ability to disable time tracking"
platform: cloud
product: jiracloud
category: devguide
subcategory: changelog
date: "2019-07-15"
---

# Deprecation Notice: Removal of the ability to disable time tracking

We are deprecating the ability to disable time tracking in Jira Cloud via REST APIs on 16 June 2019.

Support for these APIs will be removed after 16 Jan 2020.

## What changed and why?

We are no longer supporting the ability to disable time tracking in the Jira Cloud UI and Jira Cloud REST API.

We have found that only a very small number of people have this setting disabled. Removing this setting will improve the experience for configuring time tracking by removing an unnecessary option.

Upcoming changes:

## Jira Cloud UI

* The time tracking settings page will now longer allow time tracking to be disabled.

## Jira Cloud REST API

* [Get global settings](/cloud/jira/platform/rest/v3/#api-rest-api-3-configuration-get) won’t return timeTrackingEnabled.
* [Disable time tracking](/cloud/jira/platform/rest/v3/#api-rest-api-3-configuration-timetracking-delete) is deprecated.

## What are the alternatives?

Projects are able to control time tracking by associating or disassociating the time tracking field.

[For classic projects, you will need to add the fields to the screen.](https://confluence.atlassian.com/adminjiracloud/defining-a-screen-776636475.html)

[For next-gen projects, drag the field onto your issue type.](https://confluence.atlassian.com/jirasoftwarecloud/customize-an-issue-s-fields-in-next-gen-projects-951392551.html)