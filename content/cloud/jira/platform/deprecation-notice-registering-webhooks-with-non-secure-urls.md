---
title: "Deprecation notice - Registering webhooks with non-secure URLs"
platform: cloud
product: jiracloud
category: devguide
subcategory: updates
date: "2018-07-31"
---

# Deprecation notice - Registering webhooks with non-secure URLs

{{% note %}}
#### The deprecation period for these changes began on 31 July 2018.

We plan to remove support for non-secure URLs for newly registered webhooks by 31 December 2018.
{{% /note %}}

To help improve security for our customers, registering webhooks with non-https URLs is being deprecated. Secure URLs will be required when creating new webhooks later in 2018 in accordance with the [Atlassian REST API policy].

## What is changing?

If you have a script, integration, or app that registers webhooks using the Jira Cloud API, you should update it to only support HTTPS (secure) URLs as soon as possible. Depending on your implementation, this may involve adding data validation or UI and UX changes to your app.

Furthermore, if you have an app or service that receives webhook event data, it should also be running on a web server that has HTTPS enabled and a signed SSL/TLS certificate.

[Learn more about SSL, and how to get a free SSL/TLS certificate].

## Why is Atlassian making this change?

Using non-secure protocols for webhooks creates security risks for our customers. HTTPS helps prevent intruders from tampering with the webhook event data that's being sent between Atlassian and your app.

## Are self-signed SSL/TLS certificates supported?

Self-signed certificates are not supported for webhooks. The receiving web server must have a valid SSL/TLS certificate, signed by a globally trusted certificate authority.

[Learn more about SSL, and how to get a free SSL/TLS certificate].

## Which APIs and methods will be effected?

After the deprecation period, the following Jira Cloud REST API endpoint will return a 400 (Bad Request) on all webhook registration (POST) requests that contain a URL that is not HTTPS.

* `/rest/webhooks/1.0/webhook`

Also note that webhooks created via the product UI (from Jira Settings > System > Advanced) will also require HTTPS URLs following the deprecation period.

## Will existing webhooks that are not using HTTPS URLs stop working?

At this time, we are not planning to stop supporting pre-existing registered webhooks with non-secure URLs, but this is subject to change. If your app receives webhook data at a non-HTTPS URL, we strongly encourage you to migrate all existing webhooks to secure URLs.

## How can I be notified when these authentication methods will be removed?

On this page, as well as on the [developer community], we will soon provide links to issues that you can watch.

[developer community]: https://community.developer.atlassian.com/c/Jira/jira-cloud
[Atlassian REST API policy]: /platform/marketplace/atlassian-rest-api-policy/
[Learn more about SSL, and how to get a free SSL/TLS certificate]: https://letsencrypt.org/
