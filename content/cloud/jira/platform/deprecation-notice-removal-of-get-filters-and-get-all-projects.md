---
title: "Deprecation notice - Removal of non paginated projects and filters endpoints"
platform: cloud
product: jiracloud
category: devguide
subcategory: updates
date: "2018-10-19"
---

# Deprecation notice - Removal of non-paginated project and filter endpoints from REST API v2

## Deprecation notice date: 19 October 2018

In order to guarantee stability and performance of Jira Cloud, we are constantly reviewing Jira REST APIs 
to make sure their design enables and encourages good practices. 

Starting 1 April 2019, you must use the paginated endpoints for filters and projects. 
The paginated endpoints are more scalable and can deliver lower response times compared to the non-paginated ones.

## Replacement

Before the end of the deprecation period, you must do all of the following:

- replace all calls to [GET /rest/api/2/project](/cloud/jira/platform/rest/v2/#api-rest-api-2-project-get) 
with [GET /rest/api/2/project/search](/cloud/jira/platform/rest/v2/#api-rest-api-2-project-search-get)

- replace all calls to [GET /rest/api/2/filter](/cloud/jira/platform/rest/v2/#api-rest-api-2-filter-get) 
with [GET /rest/api/2/filter/search](/cloud/jira/platform/rest/v2/#api-rest-api-2-filter-search-get)

## Need help

If you need help with this change, ask in the
[Jira Cloud Development](https://community.developer.atlassian.com/c/Jira/jira-cloud)
forum in the [Developer Community](https://community.developer.atlassian.com/).