---
title: "Dynamic modules"
platform: cloud
product: jiracloud
category: devguide
subcategory: blocks
date: "2019-08-13"
---
{{% include path="docs/content/cloud/connect/concepts/dynamic-modules.snippet.md" %}}