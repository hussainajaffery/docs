---
title: "Home container"
platform: cloud
product: jiracloud
category: reference
subcategory: modules
date: "2017-09-28"
---
# Home container

The Jira home container in Jira Cloud implements the **Home container** pattern for 
Atlassian product navigation. It is a way for users to locate all of their work in 
Jira Cloud.

While adding links to the Jira home container is currently supported, we *strongly* 
encourage you to design for project-centric or user-centric experiences. This means the 
primary methods of navigation to your app's content should be via the [project sidebar] 
or [user profile menu]. Only add a link to the home container as a last resort.

The home container location only supports `web items`, however you can create additional 
web items that reference the first web item's key as their location in order to support 
nested navigation.

### Available locations

For web items:

* `system.top.navigation.bar`
* Key of the initial web item with the `system.top.navigation.bar`

![Jira home container overview](/cloud/jira/platform/images/jira-home-container.png)

### Sample descriptor JSON

``` json
"modules": {
    "webItems": [
        {
            "key": "example-section-link",
            "location": "system.top.navigation.bar",
            "name": 
                { "value": "App Toolkit" },
            "url": "http://www.example.com",
        }
    ]
}
```

[Atlassian product navigation]: https://atlassian.design/product/components/navigation/
[project sidebar]: /cloud/jira/platform/jira-project-sidebar
[user profile menu]: /cloud/jira/platform/user-profile-menu