---
title: "Integrating with Jira Cloud"
platform: cloud
product: jiracloud
category: devguide
subcategory: intro
aliases:
- /jiracloud/integrating-with-jira-cloud-43648301.html
- /jiracloud/integrating-with-jira-cloud-43648301.md    
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?pageId=43648301
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?pageId=43648301
confluence_id: 43648301
date: "2017-09-29"
---
# Integrating with Jira Cloud

![Jira cloud integration graphic](/cloud/jira/platform/images/jira-integration.png)

Welcome to Jira Cloud development! This overview will cover everything you need to know to integrate with Jira Cloud. This includes the Atlassian Connect framework, which is used to integrate with Atlassian Cloud applications, as well as Jira features and services that you can use when building an app.

{{% tip title="Hello world"%}}If you already know the theory and want to jump straight into development, read our [Getting started guide](/cloud/jira/platform/getting-started) to build your first Jira Cloud app.{{% /tip %}}

{{% include path="docs/content/cloud/jira/platform/jira-cloud-api-introduction.snippet.md" %}}

## Next steps

Ready to get hands-on with Jira Cloud development? Read our [Getting started guide](../getting-started) to learn how to set up a development environment and build an app.
