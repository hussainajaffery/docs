---
title: "Internationalization"
platform: cloud
product: jiracloud
category: devguide
subcategory: blocks
date: "2017-08-24"
---
{{% include path="docs/content/cloud/connect/concepts/internationalization.snippet.md" %}}