---
title: "Jira entity properties"
platform: cloud
product: jiracloud
category: devguide
subcategory: blocks
aliases:
- /jiracloud/jira-entity-properties-39988397.html
- /jiracloud/jira-entity-properties-39988397.md
confluence_id: 39988397
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988397
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988397
date: "2019-08-13"
---
{{% include path="docs/content/cloud/connect/concepts/jira-entity-properties.snippet.md" %}}
