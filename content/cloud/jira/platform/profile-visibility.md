---
title: "Profile visibility and apps"
platform: cloud
product: jiracloud
category: devguide
subcategory: learning
date: "2019-05-14"
---

{{% include path="docs/content/cloud/connect/guides/profile-visibility.snippet.md" %}}
