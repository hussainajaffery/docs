---
title: Scopes
platform: cloud
product: jiracloud
category: devguide
subcategory: blocks
aliases:
- /cloud/jira/platform/scopes.html
- /cloud/jira/platform/scopes.md
date: "2017-08-25"
---
{{% include path="docs/content/cloud/connect/reference/jira-scopes.snippet.md" %}}