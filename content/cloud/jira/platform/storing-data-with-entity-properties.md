---
title: Storing data with entity properties
platform: cloud
product: jiracloud
category: devguide
subcategory: learning
date: "2019-08-13"
aliases:
- /cloud/jira/platform/storing-data-without-a-database.html
- /cloud/jira/platform/storing-data-without-a-database.md
---
{{% include path="docs/content/cloud/connect/concepts/storing-jira-data-with-entity-properties.snippet.md" %}}