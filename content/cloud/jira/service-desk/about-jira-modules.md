---
title: "About Jira Service Desk modules"
platform: cloud
product: jsdcloud
category: reference
subcategory: modules
date: "2017-09-29"
---

# About Jira Service Desk modules

Jira modules allow apps to extend the functionality of the Jira platform or a Jira application. Jira 
modules are commonly used to extend the user interface by adding links, panels, or pages. However, some 
Jira modules can also be used to extend other parts of Jira, like permissions and workflows. Jira Service 
Desk also has its own modules for agent views, customer portals, and more (see below).

## Using Jira modules

You can use a Jira module by declaring it in your app descriptor (under `modules`), with the appropriate 
properties. For example, the following code adds the `generalPages` module at the `system.top.navigation.bar` 
location to your app, which adds a link in the [home container](/cloud/jira/service-desk/home-container).

**atlassian-connect.json**

``` java
...
"modules": {
          "generalPages": [
              {
                  "key": "activity",
                  "location": "system.top.navigation.bar",
                  "name": {
                      "value": "Activity"
                  }
              }
          ]
      }
...
```

There are two types of modules: basic iframes that allow you to display content in different locations 
in Jira, and more advanced modules that let you provide advanced Jira-specific functionality.

{{% include path="docs/content/cloud/jira/platform/modules.snippet.md" %}}

## Jira Service Desk modules

-   [Agent view](/cloud/jira/service-desk/agent-view)
-   [Customer portal](/cloud/jira/service-desk/customer-portal)
-   [Request create property panel](/cloud/jira/service-desk/request-create-property-panel)
-   [Automation action](/cloud/jira/service-desk/automation-action)

