---
title: "Automation action"
platform: cloud
product: jsdcloud
category: reference
subcategory: modules
aliases:
    - /jiracloud/jira-service-desk-automation-action-module-41222507.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=41222507
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=41222507
confluence_id: 41222507
date: "2017-09-11"
---
# Automation action

This pages lists the Jira Service Desk modules for the Automation Framework.

## Automation action module

The automation action module enables apps to extend the Automation Framework in Jira Service Desk by providing their own "THEN" actions. See [Guide - Implementing automation actions].

#### Module type
`automationActions`

#### Sample JSON
``` json
"modules": {
    "automationActions": [
        {
            "key": "my-automation-action",
            "name": {
                "value": "My automation action"
            },
            "configForm": {
                "url": "/configForm"
            },
            "webhook": {
                "url": "/webhook"
            }
        }
    ]
}
```

#### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the app, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`name`

-   **Type**: [i18n Property]
-   **Required**: yes
-   **Description**: A human readable name. This name will be the name visible to the user in the "Add action" screen.

`webhook`

-   **Type**: [URL]
-   **Required**: yes
-   **Description**: The URL of the app resource that will receive the webhook POST request once the action is triggered. This URL must be relative to the app's `baseUrl`. See [Webhook] for more information.

`requires`

-   **Type**: `[ string, ... ]`
-   **Required**: no
-   **Allowed values**: issue, comment, user
-   **Default**: `[ "issue" ]`
-   **Description**: A list specifying the items of information that this action "requires". See [Requires] for more information.

`configForm`

-   **Type**: [URL]
-   **Required**: no
-   **Description**: The URL of the app resource that provides the content of the configuration form for this action. This URL must be relative to the app's `baseUrl`. See [Configuration form] for more information.

## Requires

Different WHEN handlers in automation "provide" different items of information. For example, an `Issue created` WHEN handler provides `issue` - the issue that was created and `user` - the user that created the issue, but does not provide `comment`.
Another example is `Comment added` which provides `issue` - the issue that the comment was added to, `comment` - the comment that was added, and `user` - the user that added the comment.

This "provides" and "requires" relationship affects the creation of rules in two ways:

- If your THEN action "requires" an item of information that a user-selected WHEN handler does not "provide", your action will not appear in the UI.
- For an existing rule, if a WHEN handler is changed to something that no longer satisfies the "requires" of your THEN action, a validation error is shown to the user.

For example, if your action "requires" issue and comment, then it will only appear in the UI for WHEN handlers that provide at least these two items of information.

## Configuration form

The app can provide an optional configuration form (an HTML page) which defines the appearance and logic of the action. This form handles things like UI layout, client-side validation, and error message rendering.

The form should be similar to a [Web Panel], but in addition, the form needs to use [Events] and the [Atlassian Connect JavaScript API] to communicate with Jira Service Desk.

### Events

The app will need to listen to the following events that Jira Service Desk will trigger and respond by calling the corresponding API method with the response.

-   `jira-servicedesk-automation-action-serialize`
    -   Triggered when configuration is valid and is about to be saved
    -   `serialize(object)` must be called with the result of this event

-   `jira-servicedesk-automation-action-validate`
    -   Triggered when the user tries to save the form
    -   `validate(isValid)` must be called with the result of this event

### JavaScript API

Jira Service Desk provides an Automation JavaScript API module that the app will need to use in order to retrieve, store and validate its configuration.

#### Methods
 

``` js
/**
 * Retrieves the configuration object for the action.
 * Note that the configuration will be an empty object for new actions.
 *
 * @param {Function} callback - the callback that handles the response.
 */
 getConfig(callback)
 
/**
 * Provides Jira Service Desk with the configuration object of the form to be serialized and stored. 
 * 
 * Must only be called in response to the jira-servicedesk-automation-action-serialize event
 * @param {Object} object - A map containing the configuration of the form. The map must be a simple (non-nested) object.
 */
 serialize(object)
 
/**
 * Provides Jira Service Desk with the result of the validation of the configuration.
 * If called with "false", the user will not be able to save the configuration form. Ideally, this would be the time the 
 * app would render an error message notifying the user that the configuration is invalid.
 *
 * Must only be called in response to the jira-servicedesk-automation-action-validate event
 * @param {Boolean} isValid - Whether the configuration is valid and can be saved
 *       
 */
 validate(isValid)
```

### Example

This is an example script that for a simple form with just one configuration field "`#my-form-field`".

``` js
$(function () {
    var automation = new AP.jiraServiceDesk.Automation();


    automation.getConfig(function(config) {
        if (config) { // config is an empty object if this is a new action
            $("#my-form-field").val(config.myFormField);
        }
    });

    AP.events.on("jira-servicedesk-automation-action-serialize", function() {
        automation.serialize({
            myFormField: $("#my-form-field").val()
        });
    });

    AP.events.on("jira-servicedesk-automation-action-validate", function() {
        var isValid = true;
        
        if (!$("#my-form-field").val()) {
            renderErrorMessage("my form field cannot be empty");
            isValid = false;
        }

        automation.validate(isValid);
    });
});
```

 

## Webhook

When the action is triggered, the webhook resource specified by the app will receive a POST request. The JSON payload of the request will contain the following fields:

-   `timestamp`
    -   Time the webhook payload was created in milliseconds.
-   `issue`
    -   The Jira representation of the issue. This is the same shape returned by the [Jira REST API](/cloud/jira/platform/rest/#api-issue-issueIdOrKey-get).
-   `comment`
    -   The Jira representation of the comment (if it is available). This is the same shape returned by the [Jira REST API](/cloud/jira/platform/rest/#api-issue-issueIdOrKey-comment-id-get).
-   `user`
    -   The Jira representation of the user (if it is available). This is the same shape returned by the [Jira REST API](/cloud/jira/platform/rest/#api-user-get).
-   `action`
    -   Contains details about the action that was triggered. This includes the action configuration that was saved previously.

### Example

Continuing the example above, the payload might look like this:

``` js
{
    "timestamp": 1461049397396,
    "issue": {
        "id": "12345",
        "key": "JRA-678"
        "fields": { ... }
    },
    "action": {
        "configuration": {
            "myFormField": "Lorem Ipsum"
        }
    }
}
```

  [Guide - Implementing automation actions]: /cloud/jira/service-desk/implementing-automation-actions
  [URL]: /cloud/jira/platform/modules/url
  [Requires]: #requires
  [Configuration form]: #configuration-form
  [Webhook]: #webhook
  [Web Panel]: /cloud/jira/service-desk/modules/web-panel/
  [Events]: /cloud/jira/service-desk/jsapi/events/
  [Atlassian Connect JavaScript API]: /cloud/jira/service-desk/about-the-javascript-api/

