---
title: "Connect API migration"
platform: cloud
product: jsdcloud
category: devguide
subcategory: learning
date: "2018-09-01"
aliases:
- /cloud/jira/service-desk/connect-api-migration.html
- /cloud/jira/service-desk/connect-api-migration.md
---
{{% include path="docs/content/cloud/connect/concepts/connect-api-migration.snippet.md" %}}
