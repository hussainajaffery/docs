---
title: "Frameworks and tools"
platform: cloud
product: jsdcloud
category: devguide
subcategory: intro
date: "2017-09-11"
---
{{% include path="docs/content/cloud/connect/concepts/jira-and-confluence-connect-frameworks.snippet.md" %}}

## Developer Tools

[Jria web fragment finder](https://marketplace.atlassian.com/plugins/com.wittified.webfragment-finder)  
The web fragment finder is an app which loads a *Web Fragment*: Web Item, Web Section, Web Panel, in all available 
Jira locations. Web Fragments contain a unique location, making it easier to identify the right extension points 
for your app. 

[Connect inspector](https://connect-inspector.prod.public.atl-paas.net/)  
The Connect inspector is an extremely useful tool allowing developers to watch live lifecycle and webhook events 
in your web browser. The inspector allows you to generate a temporary Atlassian Connect app you install into your 
Cloud Development Environment. It will live for three days and store any lifecycle and webhook events that it receives. 

[Entity property tool](https://marketplace.atlassian.com/plugins/com.atlassian.connect.entity-property-tool.prod)  
In Jira, you can store data against the host product without a backend. Learn about 
[storing data with entity properties](/cloud/jira/platform/storing-data-with-entity-properties/). This kind of hosted 
data storage is implemented via Entity Properties and the Entity Property tool makes it trivial to create, read, 
update, and delete Entity Properties in Jira. 

[JSON descriptor validator](https://atlassian-connect-validator.herokuapp.com/validate)  
This validator will check that your descriptor is syntactically correct. Paste the JSON content of your 
app descriptor in the descriptor field, and select the Atlassian product you want to validate. 
	
[JWT decoder](http://jwt-decoder.herokuapp.com/jwt/decode)  
An encoded JWT token can be opaque. Use this handy tool to decode JWT tokens and inspect their content. 
Just paste the full URL of the resource you are trying to access in the URL field, including the JWT token. For example: 


``` text
https://example.atlassian.net/path/to/rest/endpoint?jwt=token
```

[Jira cloud-to-server plugin converter](https://github.com/minhhai2209/jira-plugin-converter)  
This is a tool to convert a Jira Cloud app's descriptor to a Jira Server app. The remote server 
is shared between both. In this way, developers can build apps for Jira Cloud first, then generate apps 
for Jira Server later.