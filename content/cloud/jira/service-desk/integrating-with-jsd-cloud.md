---
title: "Integrating with Jira Service Desk Cloud"
platform: cloud
product: jsdcloud
category: devguide
subcategory: intro
aliases:
- /jiracloud/jira-service-desk-cloud-development-39981106.html
- /jiracloud/jira-service-desk-cloud-development-39981106.md
confluence_id: 39981106
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39981106
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39981106
date: "2017-09-11"
---
# Integrating with Jira Service Desk Cloud

Welcome to Jira Service Desk Cloud development! This overview will cover everything you need to know to integrate with Jira Cloud. This includes the Atlassian Connect framework, which is used to integrate with Atlassian Cloud applications, as well as Jira Service Desk features and services that you can use when building an app.

{{% tip title="Hello world"%}}If you already know the theory and want to jump straight into development, read our [Getting started guide](../getting-started) to build your first Jira Cloud app.{{% /tip %}}

## What is Jira Service Desk? <a name="overview"></a>

Jira Service Desk is primarily used as a service solution, from asset management to DevOps. A diverse range of IT teams use Jira Service Desk, including support desk teams, operations teams, and more. With over 15,000 of these teams using Jira Service Desk, there's plenty of potential to extend it. Jump in and get started!

{{% youtube 5u62pSDVB_I %}} 

 If you haven't used Jira Service Desk before, check out the [product overview](https://www.atlassian.com/software/jira/service-desk) for more information.

## Jira Service Desk Cloud and Atlassian Connect

If you want to integrate with any Jira Cloud product, including Jira Service Desk Cloud, then you should use Atlassian Connect. Atlassian Connect is an extensibility framework that handles discovery, installation, authentication, and seamless integration into the Jira UI. An Atlassian Connect app could be an integration with another existing service, new features for Jira, or even a new product that runs within Jira. 

If you haven't used Atlassian Connect before, check out the [Getting started guide](../getting-started). This guide will help you learn how to set up a development environment and build a Jira Cloud app.

## Building blocks for integrating with Jira Service Desk Cloud

The three building blocks of integrating with Jira Service Desk are the REST API, webhooks, and modules.

### Jira Service Desk Cloud REST API

|              |                 |
|--------------|-----------------|
| ![](/illustrations/atlassian-software-47.png) | The Jira Service Desk Cloud REST API lets your app communicate with Jira Service Desk Cloud. For example, using the REST API, you can retrieve a queue's requests to display in your app or create requests from phone calls.<br>See the [Jira Service Desk Cloud REST API](/cloud/jira/service-desk/rest/) reference for details.<br>*Note, Jira Service Desk is built on the Jira platform, so you can also use the [Jira Cloud platform REST API](/cloud/jira/platform/rest/) to interact with Jira Service Desk Cloud.* |

### Webhooks and automation rules

|              |                 |
|--------------|-----------------|
| ![](/illustrations/atlassian-software-46.png) | Apps and applications can react to conditions/events in Jira Service Desk via automation rules. You can implement an "automation action" that performs actions in a remote system as part of an automation rule. An automation rule can also be configured to fire a webhooks that notifies your app or application.<br> For more information, see [Automation webhooks](/cloud/jira/service-desk/automation-webhooks).|


### Jira Service Desk modules

|              |                 |
|--------------|-----------------|
| ![](/illustrations/atlassian-software-52.png) | A module is simply a UI element, like a tab or a menu. Jira Service Desk UI modules allow apps to interact with the Jira Service Desk UI. For example, your app can use a Jira Service Desk UI module to add a panel to the top of customer portals.<br>For more information, see [About Jira modules](/cloud/jira/service-desk/about-jira-modules).|


## Jira Service Desk Cloud and the Jira platform

Jira Service Desk is an application built on the Jira platform. The Jira platform provides a set of base functionality that is shared across all Jira applications, like issues, workflows, search, email, and more. A Jira application is an extension of the Jira platform that provides specific functionality. For example, Jira Service Desk adds customer request portals, support queues, SLAs, a knowledge base, and automation.

This means that when you develop for Jira Service Desk, you are actually integrating with the Jira Service Desk application as well as the Jira platform. The Jira Service Desk application and Jira platform each have their own REST APIs, webhook events, and web fragments. 

Read the [Jira Cloud platform documentation](/cloud/jira/platform/integrating-with-jira-cloud) for more information.

## Looking for inspiration? <a name="inspiration"></a>

If you are looking for ideas on building the next Jira Service Desk Cloud integration, the following use cases and examples may help.

Here are some common Jira Service Desk use cases:

-   **Support help desk**: An easy way to provide support to anyone in the organization with IT requests such as hardware (laptop) requests, or software requests. 
-   **ITSM/ITIL:** More advanced IT teams want to use a service solution to support ITSM and ITIL processes, including incident, problem, and change management. 
-   **Asset management:** IT teams want to discover, control, monitor, and track key IT assets such as hardware and servers. 
-   **DevOps:** Developer, Operations, and IT teams can use Jira Service Desk to collaborate together and solve problems faster. 
-   **Business teams:** Finance and HR teams may want to use Jira Service Desk to collect requests from anyone in the organization. 

Here are a few examples of what you can build on top of Jira Service Desk:

- **Customer portal customization** -- Jira Service Desk provides an intuitive customer portal that makes it easy for non-technical end users to interact with service teams like IT and support. By extending this, you can build a completely tailored interface for the customer portal that matches your company's branding.
- **Collect requests outside of Jira Service Desk** -- Build functionality to create requests on behalf of customers in any number of ways. For example, integrate it into the support section of your website, or have a get help menu on your mobile app, or hook up alerts from a system monitoring tool to create incidents in Jira Service Desk.
- **SLA integration** -- Jira Service Desk introduces the notion of service level agreements (SLAs) by letting teams accurately measure and set goals based on time metrics, e.g. time to assign, time to respond, time to resolution. With the Jira Service Desk REST API, you can now get detailed SLA information and create your own reports. 
- **Telephony integration** -- Create requests based on incoming voice calls by integrating your telephony system with Jira Service Desk, via the REST API.
- **Supplement request information** -- Add information about assets, the customer, or other relevant information to requests to make it easier for agents to solve problems and close requests.

## Next steps

Ready to get hands-on with Jira Cloud development? Read our [Getting started guide](../getting-started) to learn how to set up a development environment and build an app.

These resources will also help you start developing:

-   [Jira Service Desk tutorials](../tutorials-and-guides/) -- Learn more about Jira Service Desk development by trying one of our hands-on tutorials.
-   [The Atlassian Developer Community](https://community.developer.atlassian.com/t/welcome-to-the-community/84) -- Join the discussion on Jira Service Desk development.
