---
title: "Request create property panel"
platform: cloud
product: jsdcloud
category: reference
subcategory: modules
date: "2017-09-11"
---
# Request create property panel

This page documents the request create property panel module for Jira Service Desk.

## Request create property panel module

The request create property panel is displayed on the request creation screen in the customer portal and enables apps to save arbitrary data during request creation as Jira issue properties. For more information on Jira entity properties see [Jira Entity Properties](../modules/entity-property).

### Module type
`serviceDeskPortalRequestCreatePropertyPanel`

### Screenshot
<img src="../images/jdev-service-desk-portal-request-create-property-panel.png"/> 

### Sample JSON
``` json
...
"modules": {
    "serviceDeskPortalRequestCreatePropertyPanels": [
        {
            "key": "sd-portal-request-create-property-content",
            "url": "/sd-portal-request-create-property-content"
        }
    ]
}
...
```

### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the app, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`url`

-   **Type**: `string`, `uri-template`
-   **Required**: yes
-   **Description**:  The URL of the app resource that provides the content. This URL must be relative to the app's base URL. Your app can receive [additional context] from the application by using variable tokens in the URL attribute.

`weight`

-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list.<br>Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

`conditions`

-   **Type**: [ [single condition], [composite condition], ... ]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

## JavaScript API

Jira Service Desk provides a JavaScript API that the app will need to use in order to store and validate its data.

### Events

Your app will need to listen for the following events that Jira Service Desk will trigger, and respond to each event by calling the corresponding API method with the response.

{{% warning %}}
Your app must call the corresponding API method immediately. There must not be any asynchronous operations done between receiving the event and calling the API method. 
{{% /warning %}} 

-   `jira-servicedesk-request-properties-serialize`
    -   Triggered when the request is valid and is about to be saved
    -   `serialize(object)` must be called with the result of this event

-   `jira-servicedesk-request-properties-validate`
    -   Triggered when the user tries to create the request
    -   `validate(isValid)` must be called with the result of this event

### Methods

{{% warning title="Using Jira issue properties"%}}

-   Choose a unique issue property key: Jira issue properties are not name-spaced, so each key must be unique to avoid clashes.
-   Validate your issue property, according to the following rules. If your app passes invalid issue properties to Jira Service Desk, the user will not be able to create the request.
    -   The key has a maximum length of 255 characters.
    -   The value must be a valid JSON Object and has a maximum size of 32 KB.
{{% /warning %}}


``` js
/**
 * Provides Jira Service Desk with the data that will be stored as issue properties. 
 * 
 * Must only be called in response to the jira-servicedesk-request-properties-serialize event
 * @param {Object} object - An object, where the key is the issue property key that will be used and a value for that issue property
 * e.g: serialize({issue_prop: 123, issue_prop_2: 456}) will result in two issue properties being created against the issue with the given values.
 * The values of the issue properties need not be primitive values but can be any JS object.
 */
 serialize(object)
 
/**
 * Provides Jira Service Desk with the result of the validation of the panel.
 * If called with "false", the user will not be able to create the request. Ideally, this would be the time the 
 * app would render an error message notifying the user that the data they might have entered in the panel is invalid.
 *
 * Must only be called in response to the jira-servicedesk-request-properties-validate event
 * @param {Boolean} isValid - Whether the panel's data is valid and can be saved
 *       
 */
 validate(isValid)
```

### Example

This is an example script for a simple panel that asks the user to enters their location in the field `#location`.
This panel will store the location data in the issue property `location_issue_property`.

``` js
$(function () {
    var requestProperties = new AP.jiraServiceDesk.RequestProperties();

    AP.events.on("jira-servicedesk-request-properties-serialize", function() {
        requestProperties.serialize({
            com_company_myapp_location_issue_property: $("#location").val()
        });
    });

    AP.events.on("jira-servicedesk-request-properties-validate", function() {
        var valid = true;

        if (!$("#location").val()) {
            $("#location").parent().append("<p class='field-error'>location can't be empty</p>");
            valid = false;
        }

        requestProperties.validate(valid);
    });
});
```

[Jira Entity Properties]: /cloud/jira/service-desk/jira-entity-properties.md
