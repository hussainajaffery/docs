---
title: Storing data with entity properties
platform: cloud
product: jsdcloud
category: devguide
subcategory: learning
date: "2019-08-13"
aliases:
- /cloud/jira/service-desk/storing-data-without-a-database.html
- /cloud/jira/service-desk/storing-data-without-a-database.md
---
{{% include path="docs/content/cloud/connect/concepts/storing-jira-data-with-entity-properties.snippet.md" %}}