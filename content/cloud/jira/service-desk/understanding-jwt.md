---
title: Understanding JWT for Connect apps
platform: cloud
product: jsdcloud
category: devguide
subcategory: securityconnect
date: "2019-08-19"
---
{{% include path="docs/content/cloud/connect/concepts/understanding-jwt.snippet.md" %}}