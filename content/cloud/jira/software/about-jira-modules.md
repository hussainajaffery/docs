---
title: "About Jira Software modules" 
platform: cloud
product: jswcloud
category: reference
subcategory: modules
aliases:
- /jiracloud/jira-software-modules-39987281.html
- /jiracloud/jira-software-modules-39987281.md
confluence_id: 39987281
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39987281
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39987281
date: "2017-09-29"
---
# About Jira Software modules

Jira modules allow apps to extend the functionality of the Jira platform or a Jira application. Jira 
modules are commonly used to extend the user interface by adding links, panels, etc. However, some Jira 
modules can also be used to extend other parts of Jira, like permissions and workflows. Jira Software 
also has its own modules for boards (see below).

## Using Jira modules

You can use a Jira module by declaring it in your app descriptor (under `modules`), with the appropriate 
properties. For example, the following code adds the `generalPages` module at the `system.top.navigation.bar` 
location to your app, which adds a link in the [home container](/cloud/jira/software/home-container).

**atlassian-connect.json**

``` java
...
"modules": {
          "generalPages": [
              {
                  "key": "activity",
                  "location": "system.top.navigation.bar",
                  "name": {
                      "value": "Activity"
                  }
              }
          ]
      }
...
```

There are two types of modules: basic iframes that allow you to display content in different locations 
in Jira, and more advanced modules that let you provide advanced Jira-specific functionality.

{{% include path="docs/content/cloud/jira/platform/modules.snippet.md" %}}
