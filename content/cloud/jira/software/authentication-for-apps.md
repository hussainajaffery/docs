---
title: "Authentication for apps"
platform: cloud
product: jswcloud
category: devguide
subcategory: security
date: "2019-08-19"
---
{{% include path="docs/content/cloud/connect/concepts/authentication.snippet.md" %}}