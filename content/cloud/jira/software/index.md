---
title: "Jira Software Cloud API introduction"
platform: cloud
product: jswcloud
category: devguide
subcategory: intro
aliases:
- /jiracloud/jira-software-cloud-development-39981104.html
- /jiracloud/jira-software-cloud-development-39981104.md
confluence_id: 39981104
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39981104
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39981104
date: "2018-05-24"
---

# Integrating with Jira Software Cloud

![Jira cloud integration graphic](/cloud/jira/platform/images/jira-integration.png)

Welcome to Jira Software Cloud development! This overview will cover everything you need to know to integrate with Jira Software Cloud. This includes the Atlassian Connect framework, which is used to integrate with Atlassian Cloud applications, as well as Jira features and services that you can use when building an app.

{{% warning title="Important! API changes to improve user privacy"%}}
<p>On 24 May 2018 <a href="/cloud/jira/platform/api-changes-for-user-privacy-announcement">we announced upcoming API changes</a> to Jira Cloud REST APIs, webhooks, and extension points to support user privacy. Before continuing, please ensure you have read the <a href="/cloud/jira/platform/api-changes-for-user-privacy-announcement">change notice</a>.</p>{{% /warning %}}

{{% tip title="Hello world"%}}If you already know the theory and want to jump straight into development, read our [Getting started guide](../getting-started) to build your first Jira Cloud app.{{% /tip %}}

{{% include path="docs/content/cloud/jira/platform/jira-cloud-api-introduction.snippet.md" %}}

## Integrating with on-premises tools

Jira Software Cloud provides OAuth 2.0 credentials that you can use to integrate Jira Software Cloud with on-premises (self-hosted) tools. Currently, you can integrate with build and deployment tools, such as Jenkins. In future, this mechanism will also support development information from tools like repository managers. See [Integrating Jira Software Cloud with on-premises tools](../integrate-jsw-cloud-with-onpremises-tools) for details.

## Looking for inspiration? <a name="inspiration"></a>

If you are looking for ideas on building the next Jira Software Cloud integration, here are a few examples of what you can build on top of Jira Software Cloud:

-   [Retrospective Tools for Jira](https://marketplace.atlassian.com/apps/1213882/retrospective-tools-for-jira) -- This app provides a number of tools for reviewing your project history, and with the Jira Software API they're able to let you filter by board, or even overlay your swimlanes.
-   [Epic Sum Up](https://marketplace.atlassian.com/apps/1213091/epic-sum-up) -- This app adds a panel to the issue view that allows you to review your epic progress or every issue within the epic.
-   [Tempo Planner for Jira](https://marketplace.atlassian.com/apps/1211881/tempo-planner) -- This app fetches information from boards, epics, and backlogs using the Jira Software API, and lets you plan your work by team member.

## Next steps

Ready to get hands-on with Jira Cloud development? Read our [Getting started guide](../getting-started) to learn how to set up a development environment and build an app.

These resources will also help you start developing:

-   [Jira Software Cloud tutorials](/cloud/jira/software/tutorials-and-guides/) -- Learn more about Jira Software development by trying one of our hands-on tutorials.
-   [The Atlassian Developer Community](https://community.developer.atlassian.com/t/welcome-to-the-community/84) -- Join the discussion on Jira Software development.
