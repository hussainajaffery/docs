---
title: Issue view UI locations
platform: cloud
product: jswcloud
category: reference
subcategory: modules
date: "2017-08-24"
---

{{% reuse-page path="docs/content/cloud/jira/platform/issue-view-ui-locations.md" %}}