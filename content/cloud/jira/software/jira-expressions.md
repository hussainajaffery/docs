---
title: "Jira expressions"
platform: cloud
product: jiracloud
category: devguide
subcategory: blocks
date: "2019-07-30"
---
{{% include path="docs/content/cloud/connect/concepts/jira-expressions.snippet.md" %}}