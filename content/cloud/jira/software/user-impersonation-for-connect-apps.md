---
title: "OAuth 2.0 - JWT bearer token authorization grant type"
platform: cloud
product: jswcloud
category: devguide
subcategory: security
date: "2019-08-19"
---
{{% include path="docs/content/cloud/connect/concepts/oauth2-jwt-bearer-token-authentication.snippet.md" %}}