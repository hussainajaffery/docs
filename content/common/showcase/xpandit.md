---
title: Showcase Xpand IT
logo_url: /img/logo-xpandit-white.png
logo_css_class: xpand-it-logo
heading1: "Expanding into new markets"
byline: "Atlassian Solution Partner Xpand IT scales its business with apps"
lede: "Portugal-based Xpand IT was founded in 2003 by Marco Oliveira and Pedro Gonçalves, who aimed to set themselves apart by providing IT consulting services on disruptive new technologies, including Atlassian. Now, with numerous big-name customers across Europe and the U.S. – from Mastercard to Lufthansa – the Atlassian Solution Partner has expanded into software products, releasing four popular apps in the Marketplace."
---
## Starting small

Portugal is a relatively small country, comprising just 2% of Europe's population, so Xpand IT's bread and butter has been exporting services to larger neighbors like Germany and the United Kingdom. When its ambitious founders were ready to scale the business, they looked for an opportunity that wouldn't be limited by geography.

## Scaling up

In 2012, Xpand IT was doing custom development for exporting information from Jira into mail letters, similar to Mail Merge in Microsoft Word. Recognizing a market need for issue exporting, the company decided to take it all the way and develop an app with a roadmap and feature list far beyond the customer's requirements.
 
Xpand IT contacted several big customers for beta testing, feedback, and guidance. "I think it's very important to not only consider what your competitors are doing, but to also have people that really need the product helping you to choose the right features," explains co-founder and CTO Pedro Gonçalves.
 
[Xporter for Jira](https://marketplace.atlassian.com/plugins/com.xpandit.plugins.jiraxporter/cloud/overview), the company's first app, launched not long after the Atlassian Marketplace did. Says Gonçalves: "So we decided to just try it out, see how it works, and try the billing process through Atlassian. It was a surprise for us that we started getting lots of trials and slowly customers purchasing through the Marketplace."
 
The company's main objective for the first year was to get a big number of installations, product feedback, and customer reviews to refine and improve the product while gaining visibility on the Marketplace. "Once you reach a certain number of installations and you have a good average score in reviews, it starts growing by itself. You don't actually need a big sales team," says Gonçalves.
 
Xpand IT was making a name for itself as an app developer. So when Portugal Telecom was looking for a test management solution and didn't find what it needed on the Marketplace, it challenged Xpand IT to create a product. [Xray for Jira](https://marketplace.atlassian.com/plugins/com.xpandit.plugins.xray/server/overview) was released in 2014, quickly becoming a top seller.
 
> <img class="quote-profile" src="/img/pedro-goncalves-quote.png"> "So we decided to just try it out, see how it works, and try the billing process through Atlassian. It was a surprise for us that we started getting lots of trials and slowly customers purchasing through the Marketplace."
 
With the business growth of their products in the Atlassian Marketplace, Xpand IT created a business unit named Xpand Add-ons. "Since 2015, [Xpand Add-ons](https://www.xpand-addons.com/) is the name of the team behind Xray and Xporter."
 
To compete against rivals in 2017, Xpand Add-ons is putting more energy into marketing via customer engagement and social media, as well as visiting Atlassian Summits, partner events, and user groups. Developer events, like Atlassian's Connect Weeks, have also been a key resource for learning and overcoming challenges, especially as Xpand Add-ons build out cloud versions to reach more customers.
 
"Exporting documents is rarely a mission-critical functionality, so customers don't always upgrade Xporter when they upgrade Jira. From that perspective, going to cloud and the monthly subscription model is much better," explains Gonçalves.

## Exponential growth

The Marketplace platform, which offers a built-in customer base, billing, peer ecosystem, and lots of other resources, gave Atlassian Solution Partner Xpand IT the right opportunity to productize its knowledge and scale its business. With app revenue growing from $30K to $1M in just 18 months, the company expects continued exponential growth for the foreseeable future.
 
"The thing that I'm most proud of is our customers. We didn't think it would be possible to have companies like Airbus, Boeing, Intel, Nike, Walmart, and Audi using our products. It's quite rewarding accomplishing that while working from a small country like Portugal."

{{% linkblock href="../tempo/" img="/img/clouds.png" text="Tempo" section="cloud" align="left" %}}
{{% linkblock href="../adaptavist/" img="/img/chat-bubble.png" text="Adaptavist" section="cloud" align="right" %}}
