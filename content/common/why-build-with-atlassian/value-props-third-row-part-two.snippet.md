<h4 class="center">You choose the stack</h4>

Like working with open web-based technology? Atlassian lets you use your own language, your own framework, and your own hosting solution. Our platform is agnostic, so you're free to write your code however you want.